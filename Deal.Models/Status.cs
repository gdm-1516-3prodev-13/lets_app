using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Deal.Models
{
    public enum ModelTypes
    {
        Object,
        Group
    }

    public enum StatusTypes
    {
        ModelValid,
        ModelInvalid,
        AddedSuccess,
        AddedFailure
    }

    public class Status
    {
        public StatusTypes Id { get; set; }
        public ModelTypes Type { get; set; }
        public string Description { get; set; }
    }

    public class GroupStatus : Status
    {
        public GroupStatus()
        {
            Type = ModelTypes.Group;
        }
    }
}
