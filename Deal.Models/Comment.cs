using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Deal.Models.Identity;

namespace Deal.Models
{
    public class Comment : Item
    {
        public Comment() :base()
        {
            
        }

        public Int64 Id { get; set; }
        [Required]
        public string Content { get; set; }
        
        /* Foreign Keys */
        public string UserId { get; set; }
        public Nullable<Int32> RequestId { get; set; }
        public Nullable<Int32> OfferId { get; set; }
        
        /* Virtual or Navigation Properties */
        public virtual ApplicationUser User { get; set; }
        public virtual Request Request { get; set; }
        public virtual Offer Offer { get; set; }
    }
}
