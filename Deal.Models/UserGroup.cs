using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Deal.Models.Identity;

namespace Deal.Models
{
    public class UserGroup
    {
        public UserGroup()
        {
        
        }

        public string UserId  { get; set; }
        public Int32 GroupId  { get; set; }
        
        /* Virtual or Navigation Properties */
        public virtual ApplicationUser User { get; set; }
        public virtual Group Group { get; set; }
        
    }
}
