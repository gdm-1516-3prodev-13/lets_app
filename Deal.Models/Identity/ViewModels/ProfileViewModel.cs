using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deal.Models.Identity.ViewModels
{
    public class ProfileViewModel
    {
        public ApplicationUser User { get; set; }
        public Profile Profile { get; set; }
        public IEnumerable<Group> Groups { get; set; }
        public IEnumerable<Request> Requests { get; set; }
        public IEnumerable<Offer> Offers { get; set; }
    }
}