using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc.Rendering;

namespace Deal.Models.Identity.ViewModels
{
    public class AdminUserRoleViewModel
    {
        public ApplicationRole Role { get; set; }
        public IEnumerable<ApplicationUser> UsersInRole { get; set; }
        public IEnumerable<ApplicationUser> UsersNotInRole { get; set; }
    }
}
