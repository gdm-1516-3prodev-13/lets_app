using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc.Rendering;

namespace Deal.Models.Identity.ViewModels
{
    public class AdminProfileViewModel
    {
        public ApplicationUser User { get; set; }
        public Profile Profile { get; set; }
    }
}