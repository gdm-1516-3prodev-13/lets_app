using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Deal.Models;

namespace Deal.Models.Identity
{
    public class ApplicationUser : IdentityUser
    {   
		public DateTime CreatedAt { get; set; }
		public Nullable<DateTime> UpdatedAt { get; set; }
		public Nullable<DateTime> DeletedAt { get; set; }
        
        /* Virtual or Navigation Properties */
        public virtual Profile Profile { get; set; }
        public virtual ICollection<Group> CreatedGroups { get; set; }
        public virtual ICollection<UserGroup> SubscribedGroups { get; set; }
        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<Request> Requests { get; set; }
        public virtual ICollection<Offer> Offers { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
