using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Deal.Models.Identity;

namespace Deal.Models
{
    public class Group : Item
    {
        public Group() :base()
        {
            
        }

        public Int32 Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        
        /* Foreign Keys */
        [Display(Name = "User")]
        public string UserId { get; set; }
        
        /* Virtual or Navigation Properties */
        public virtual ApplicationUser User { get; set; }
        public virtual ICollection<UserGroup> Users { get; set; }
        public virtual ICollection<Request> Requests { get; set; }
        public virtual ICollection<Offer> Offers { get; set; }
    }
}
