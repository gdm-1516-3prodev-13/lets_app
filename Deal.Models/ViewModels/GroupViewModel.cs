using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Deal.Models.Identity;

namespace Deal.Models.ViewModels
{
    public class GroupViewModel
    {
        public Group Group { get; set; }
        public bool IsSubscribed { get; set; }
        public IEnumerable<Request> Requests { get; set; }
        public IEnumerable<Offer> Offers { get; set; }
        public IEnumerable<ApplicationUser> Users { get; set; }
    }
}
