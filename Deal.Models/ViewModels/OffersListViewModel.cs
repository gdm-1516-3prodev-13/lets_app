using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Deal.Models.ViewModels
{
    public class OffersListViewModel
    {
        public IEnumerable<Offer> Offers { get; set; }
        public IEnumerable<Offer> OpenOffers { get; set; }
        public IEnumerable<Offer> CompletedOffers { get; set; }
    }
}
