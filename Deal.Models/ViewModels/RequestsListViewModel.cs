using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Deal.Models.ViewModels
{
    public class RequestsListViewModel
    {
        public IEnumerable<Request> Requests { get; set; }
        public IEnumerable<Request> OpenRequests { get; set; }
        public IEnumerable<Request> CompletedRequests { get; set; }
    }
}
