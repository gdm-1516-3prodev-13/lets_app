using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Deal.Models.Identity;

namespace Deal.Models.ViewModels
{
    public class RequestViewModel
    {
        public Request Request { get; set; }
        public Offer AcceptedOffer { get; set; }
        public Comment NewComment { get; set; }
        public IEnumerable<Offer> Offers { get; set; }
        public IEnumerable<Group> Groups { get; set; }
        public IEnumerable<ApplicationUser> Users { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<Comment> Comments { get; set; }
        
        public enum TypeBonus
        {
            Light = 5,
            Medium = 10,
            Heavy = 15
        }
    }
}
