using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Deal.Models.Identity;

namespace Deal.Models.ViewModels
{
    public class UserOverviewViewModel
    {
        public ApplicationUser User { get; set; }
        public IEnumerable<Group> MyGroups { get; set; }
        public IEnumerable<Request> MyRequests { get; set; }
        public IEnumerable<Request> MyActiveRequests { get; set; }
        public IEnumerable<Request> MyCompletedRequests { get; set; }
        public IEnumerable<Offer> MyOffers { get; set; }
        public IEnumerable<Offer> MyActiveOffers { get; set; }
        public IEnumerable<Offer> MyCompletedOffers { get; set; }
    }
}
