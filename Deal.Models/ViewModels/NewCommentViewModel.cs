using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deal.Models.ViewModels
{
    public class NewCommentViewModel
    {
        public int? Id { get; set; }
        public string UserId { get; set; }
        public string EntityType { get; set; }
        public Comment Comment { get; set; }
    }
}
