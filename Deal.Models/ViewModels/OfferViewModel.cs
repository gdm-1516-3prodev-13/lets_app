using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Deal.Models.Identity;

namespace Deal.Models.ViewModels
{
    public class OfferViewModel
    {
        public Offer Offer { get; set; }
        public Request ParentRequest { get; set; }
        public Request AcceptedRequest { get; set; }
        public Comment NewComment { get; set; }
        public IEnumerable<Group> Groups { get; set; }
        public IEnumerable<ApplicationUser> Users { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<Request> Requests { get; set; }
        public IEnumerable<Comment> Comments { get; set; }
    }
}
