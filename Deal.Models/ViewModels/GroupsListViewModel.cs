using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Deal.Models.ViewModels
{
    public class GroupsListViewModel
    {
        public IEnumerable<Group> SubscribedGroups { get; set; }
        public IEnumerable<Group> OpenGroups { get; set; }
    }
}
