using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Deal.Models.Identity;

namespace Deal.Models
{
    public class Category : Item
    {
        public Category() :base()
        {
            
        }

        public Int16 Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        
        /* Foreign Keys */
        public string UserId { get; set; }
        
        /* Virtual or Navigation Properties */
        public virtual ApplicationUser User { get; set; }
        public virtual ICollection<Request> Requests { get; set; }
        public virtual ICollection<Offer> Offers { get; set; }
    }
}
