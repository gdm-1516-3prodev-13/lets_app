using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Deal.Models.Identity;

namespace Deal.Models
{
    public class Request : Item
    {
        public Request() :base()
        {
            
        }

        public Int32 Id { get; set; }
        
        [Required]
        [StringLength(128, MinimumLength=2, ErrorMessage="The {0} must be at least {2} characters long.")]
        public string Title { get; set; }
        
        [StringLength(1024, MinimumLength=2, ErrorMessage="The {0} must be at least {2} characters long.")]
        public string Description { get; set; }
        
        [Display(Name = "Type of work")]
        public TypeBonus? Bonus { get; set; }
        
        [Required]
        [Display(Name = "Estimated Time")]
        public Int32 EstimatedTime { get; set; }
        
        [Required]
        [Display(Name = "Completed")]
        public bool IsCompleted { get; set; }
        
        /* Foreign Keys */
        [Display(Name = "ParentOffer")]
        public Nullable<Int32> OfferId { get; set; }
        
        [Required]
        [Display(Name = "Group")]
        public Int32 GroupId { get; set; }
        
        [Display(Name = "User")]
        public string UserId { get; set; }
        
        [Required]
        [Display(Name = "Category")]
        public Int16 CategoryId { get; set; }
        
        /* Virtual or Navigation Properties */
        public virtual Group Group { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual Offer ParentOffer { get; set; }
        public virtual ICollection<Offer> Offers { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
    
    public enum TypeBonus
    {
        Light = 5,
        Medium = 10,
        Heavy = 15
    }
}
