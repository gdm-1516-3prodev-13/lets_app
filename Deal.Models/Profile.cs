using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using Deal.Models.Identity;

namespace Deal.Models
{
    public class Profile
    {
        public string UserId { get; set; }
        [Display(Name="First Name")]
        public string FirstName { get; set; }
        [Display(Name="Last Name")]
		public string LastName { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public Int32 Points { get; set; }
    }
}
