using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Deal.Models.Identity;

namespace Deal.Models
{
    public class Offer : Item
    {
        public Offer() :base()
        {
            
        }

        public Int32 Id { get; set; }
        
        [Required]
        [StringLength(128, MinimumLength=2, ErrorMessage="The {0} must be at least {2} characters long.")]
        public string Title { get; set; }
        
        [StringLength(1024, MinimumLength=2, ErrorMessage="The {0} must be at least {2} characters long.")]
        public string Description { get; set; }
        
        [Required]
        [Display(Name = "Completed")]
        public bool IsCompleted { get; set; }
        
        /* Foreign Keys */
        [Display(Name = "ParentRequest")]
        public Nullable<Int32> RequestId { get; set; }
        
        [Display(Name = "User")]
        public string UserId { get; set; }
        
        [Required]
        [Display(Name = "Group")]
        public Int32 GroupId { get; set; }
        
        [Required]
        [Display(Name = "Category")]
        public Int16 CategoryId { get; set; }
        
        /* Virtual or Navigation Properties */
        public virtual Request ParentRequest { get; set; }
        public virtual ICollection<Request> Requests { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual Group Group { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
