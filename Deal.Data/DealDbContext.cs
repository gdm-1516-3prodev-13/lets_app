using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.AspNet.Identity.EntityFramework;
using Deal.Models;
using Deal.Models.Identity;

namespace Deal.Data
{
    public class DealDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public DbSet<Group> Groups { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<Offer> Offers { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }

        public DealDbContext(): base()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            // ApplicationUser
            modelBuilder.Entity<ApplicationUser>(l =>
            {  
               l.HasOne(m => m.Profile).WithOne().HasForeignKey<Profile>(m => m.UserId);
            });
            
            // Profile
            modelBuilder.Entity<Profile>(l =>
            {  
               l.ToTable("dl_profiles");
               l.HasKey(m => m.UserId);
               l.Property(m => m.FirstName).IsRequired().HasColumnType("nvarchar(64)");
               l.Property(m => m.LastName).IsRequired().HasColumnType("nvarchar(128)");
               l.Property(m => m.Description).IsRequired(false).HasColumnType("nvarchar(1024)");
               l.Property(m => m.Location).IsRequired(false).HasColumnType("nvarchar(128)");
               l.Property(m => m.Points).IsRequired();
            });

            // Request
            modelBuilder.Entity<Request>(l =>
            {
                l.ToTable("dl_requests");
                l.HasKey(m => m.Id);
                l.Property(m => m.Id).ValueGeneratedOnAdd();
                l.Property(m => m.CreatedAt).IsRequired().HasColumnType("datetime").ValueGeneratedOnAdd();
                l.Property(m => m.UpdatedAt).IsRequired(false).HasColumnType("datetime");
                l.Property(m => m.DeletedAt).IsRequired(false).HasColumnType("datetime");
                l.Property(m => m.Title).IsRequired().HasColumnType("nvarchar(128)");
                l.Property(m => m.Description).IsRequired(false).HasColumnType("nvarchar(1024)");
                l.Property(m => m.EstimatedTime).IsRequired();
                l.Property(m => m.IsCompleted).IsRequired().HasColumnType("boolean");
                l.Property(m => m.Bonus).IsRequired(false);
                
                // Group => 0 to many Requests
                l.HasOne(m => m.Group)
                .WithMany(m => m.Requests)
                .HasForeignKey(m => m.GroupId);
                
                // User => 0 to many Requests
                l.HasOne(m => m.User)
                .WithMany(m => m.Requests)
                .HasForeignKey(m => m.UserId);
                
                // Category => 0 to many Requests
                l.HasOne(m => m.Category)
                .WithMany(m => m.Requests)
                .HasForeignKey(m => m.CategoryId);
                
                // Offer => 0 to many Requests
                l.HasOne(m => m.ParentOffer)
                .WithMany(m => m.Requests)
                .HasForeignKey(m => m.OfferId);
            });
            
            // Offer
            modelBuilder.Entity<Offer>(l =>
            {
                l.ToTable("dl_offers");
                l.HasKey(m => m.Id);
                l.Property(m => m.Id).ValueGeneratedOnAdd();
                l.Property(m => m.CreatedAt).IsRequired().HasColumnType("datetime").ValueGeneratedOnAdd();
                l.Property(m => m.UpdatedAt).IsRequired(false).HasColumnType("datetime");
                l.Property(m => m.DeletedAt).IsRequired(false).HasColumnType("datetime");
                l.Property(m => m.Title).IsRequired().HasColumnType("nvarchar(128)");
                l.Property(m => m.Description).IsRequired(false).HasColumnType("nvarchar(1024)");
                l.Property(m => m.IsCompleted).IsRequired().HasColumnType("boolean").HasDefaultValue(false);
                
                // Group => 0 to many Offers
                l.HasOne(m => m.Group)
                .WithMany(m => m.Offers)
                .HasForeignKey(m => m.GroupId);
                
                // User => 0 to many Offers
                l.HasOne(m => m.User)
                .WithMany(m => m.Offers)
                .HasForeignKey(m => m.UserId);
                
                // Category => 0 to many Offers
                l.HasOne(m => m.Category)
                .WithMany(m => m.Offers)
                .HasForeignKey(m => m.CategoryId);
                
                // Request => 0 to many Offers
                l.HasOne(m => m.ParentRequest)
                .WithMany(m => m.Offers)
                .HasForeignKey(m => m.RequestId);
            });
            
            // Group
            modelBuilder.Entity<Group>(l =>
            {
                l.ToTable("dl_groups");
                l.HasKey(m => m.Id);
                l.Property(m => m.Id).ValueGeneratedOnAdd();
                l.Property(m => m.CreatedAt).IsRequired().HasColumnType("datetime").ValueGeneratedOnAdd();
                l.Property(m => m.UpdatedAt).IsRequired(false).HasColumnType("datetime");
                l.Property(m => m.DeletedAt).IsRequired(false).HasColumnType("datetime");
                l.Property(m => m.Name).IsRequired().HasColumnType("nvarchar(128)");
                l.Property(m => m.Description).IsRequired(false).HasColumnType("nvarchar(1024)");
                
                // User => 0 to many Groups
                l.HasOne(m => m.User)
                .WithMany(m => m.CreatedGroups)
                .HasForeignKey(m => m.UserId);
            });
            
            // Multiple Users in multiple Groups
            modelBuilder.Entity<UserGroup>(l =>
            {
                l.ToTable("dl_users_in_groups");
                l.HasKey(m => new { m.UserId, m.GroupId });
            });
            
            // Category
            modelBuilder.Entity<Category>(l =>
            {
                l.ToTable("dl_categories");
                l.HasKey(m => m.Id);
                l.Property(m => m.Id).ValueGeneratedOnAdd();
                l.Property(m => m.CreatedAt).IsRequired().HasColumnType("datetime").ValueGeneratedOnAdd();
                l.Property(m => m.UpdatedAt).IsRequired(false).HasColumnType("datetime");
                l.Property(m => m.DeletedAt).IsRequired(false).HasColumnType("datetime");
                l.Property(m => m.Name).IsRequired().HasColumnType("nvarchar(128)");
                l.Property(m => m.Description).IsRequired(false).HasColumnType("nvarchar(1024)");
                
                // User => 0 to many Categories
                l.HasOne(m => m.User)
                .WithMany(m => m.Categories)
                .HasForeignKey(m => m.UserId);
            });
            
            // Comment
            modelBuilder.Entity<Comment>(l => 
            {
                l.ToTable("dl_comments");
                l.HasKey(m => m.Id);
                l.Property(m => m.Id).ValueGeneratedOnAdd();
                l.Property(m => m.CreatedAt).IsRequired().HasColumnType("datetime").ValueGeneratedOnAdd();
                l.Property(m => m.UpdatedAt).IsRequired(false).HasColumnType("datetime");
                l.Property(m => m.DeletedAt).IsRequired(false).HasColumnType("datetime");
                l.Property(m => m.Content).IsRequired().HasColumnType("nvarchar(65536)");
                
                // User => 0 to many Comments
                l.HasOne(m => m.User)
                .WithMany(m => m.Comments)
                .HasForeignKey(m => m.UserId);
            });
        }
        
        public override int SaveChanges()
        {
            this.ChangeTracker.DetectChanges();

            var entries = this.ChangeTracker.Entries();

            // Add entry
            var entriesFiltered = entries.Where(e => e.State == EntityState.Added);
            foreach (var entry in entriesFiltered)
            {
                TrySetProperty(entry.Entity, "CreatedAt", DateTime.UtcNow);
            }

            // Update entry
            entriesFiltered = entries.Where(e => e.State == EntityState.Modified);
            foreach (var entry in entriesFiltered)
            {
                TrySetProperty(entry.Entity, "UpdatedAt", DateTime.UtcNow);
            }

            return base.SaveChanges();
        }
        
        private void TrySetProperty(object obj, string p, object value)
        {
            var prop = obj.GetType().GetProperty(p);
            if(prop != null && prop.CanWrite)
            {
                prop.SetValue(obj, value, null);
            }
        }
    }
}
