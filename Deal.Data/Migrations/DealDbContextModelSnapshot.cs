using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using Deal.Data;

namespace Deal.Data.Migrations
{
    [DbContext(typeof(DealDbContext))]
    partial class DealDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348");

            modelBuilder.Entity("Deal.Models.Category", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<DateTime?>("DeletedAt")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<string>("Description")
                        .HasAnnotation("Relational:ColumnType", "nvarchar(1024)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("Relational:ColumnType", "nvarchar(128)");

                    b.Property<DateTime?>("UpdatedAt")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "dl_categories");
                });

            modelBuilder.Entity("Deal.Models.Comment", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Content")
                        .IsRequired()
                        .HasAnnotation("Relational:ColumnType", "nvarchar(65536)");

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<DateTime?>("DeletedAt")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<int?>("OfferId");

                    b.Property<int?>("RequestId");

                    b.Property<DateTime?>("UpdatedAt")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "dl_comments");
                });

            modelBuilder.Entity("Deal.Models.Group", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<DateTime?>("DeletedAt")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<string>("Description")
                        .HasAnnotation("Relational:ColumnType", "nvarchar(1024)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("Relational:ColumnType", "nvarchar(128)");

                    b.Property<DateTime?>("UpdatedAt")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "dl_groups");
                });

            modelBuilder.Entity("Deal.Models.Identity.ApplicationRole", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Description");

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasAnnotation("Relational:Name", "RoleNameIndex");

                    b.HasAnnotation("Relational:TableName", "AspNetRoles");
                });

            modelBuilder.Entity("Deal.Models.Identity.ApplicationUser", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<DateTime?>("UpdatedAt");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasAnnotation("Relational:Name", "EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .HasAnnotation("Relational:Name", "UserNameIndex");

                    b.HasAnnotation("Relational:TableName", "AspNetUsers");
                });

            modelBuilder.Entity("Deal.Models.Offer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<short>("CategoryId");

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<DateTime?>("DeletedAt")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<string>("Description")
                        .HasAnnotation("MaxLength", 1024)
                        .HasAnnotation("Relational:ColumnType", "nvarchar(1024)");

                    b.Property<int>("GroupId");

                    b.Property<bool>("IsCompleted")
                        .HasAnnotation("Relational:ColumnType", "boolean")
                        .HasAnnotation("Relational:DefaultValue", "False")
                        .HasAnnotation("Relational:DefaultValueType", "System.Boolean");

                    b.Property<int?>("RequestId");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 128)
                        .HasAnnotation("Relational:ColumnType", "nvarchar(128)");

                    b.Property<DateTime?>("UpdatedAt")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "dl_offers");
                });

            modelBuilder.Entity("Deal.Models.Profile", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("Description")
                        .HasAnnotation("Relational:ColumnType", "nvarchar(1024)");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasAnnotation("Relational:ColumnType", "nvarchar(64)");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasAnnotation("Relational:ColumnType", "nvarchar(128)");

                    b.Property<string>("Location")
                        .HasAnnotation("Relational:ColumnType", "nvarchar(128)");

                    b.Property<int>("Points");

                    b.HasKey("UserId");

                    b.HasAnnotation("Relational:TableName", "dl_profiles");
                });

            modelBuilder.Entity("Deal.Models.Request", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("Bonus");

                    b.Property<short>("CategoryId");

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<DateTime?>("DeletedAt")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<string>("Description")
                        .HasAnnotation("MaxLength", 1024)
                        .HasAnnotation("Relational:ColumnType", "nvarchar(1024)");

                    b.Property<int>("EstimatedTime");

                    b.Property<int>("GroupId");

                    b.Property<bool>("IsCompleted")
                        .HasAnnotation("Relational:ColumnType", "boolean");

                    b.Property<int?>("OfferId");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 128)
                        .HasAnnotation("Relational:ColumnType", "nvarchar(128)");

                    b.Property<DateTime?>("UpdatedAt")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "dl_requests");
                });

            modelBuilder.Entity("Deal.Models.UserGroup", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<int>("GroupId");

                    b.HasKey("UserId", "GroupId");

                    b.HasAnnotation("Relational:TableName", "dl_users_in_groups");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasAnnotation("Relational:TableName", "AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasAnnotation("Relational:TableName", "AspNetUserRoles");
                });

            modelBuilder.Entity("Deal.Models.Category", b =>
                {
                    b.HasOne("Deal.Models.Identity.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Deal.Models.Comment", b =>
                {
                    b.HasOne("Deal.Models.Offer")
                        .WithMany()
                        .HasForeignKey("OfferId");

                    b.HasOne("Deal.Models.Request")
                        .WithMany()
                        .HasForeignKey("RequestId");

                    b.HasOne("Deal.Models.Identity.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Deal.Models.Group", b =>
                {
                    b.HasOne("Deal.Models.Identity.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Deal.Models.Offer", b =>
                {
                    b.HasOne("Deal.Models.Category")
                        .WithMany()
                        .HasForeignKey("CategoryId");

                    b.HasOne("Deal.Models.Group")
                        .WithMany()
                        .HasForeignKey("GroupId");

                    b.HasOne("Deal.Models.Request")
                        .WithMany()
                        .HasForeignKey("RequestId");

                    b.HasOne("Deal.Models.Identity.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Deal.Models.Profile", b =>
                {
                    b.HasOne("Deal.Models.Identity.ApplicationUser")
                        .WithOne()
                        .HasForeignKey("Deal.Models.Profile", "UserId");
                });

            modelBuilder.Entity("Deal.Models.Request", b =>
                {
                    b.HasOne("Deal.Models.Category")
                        .WithMany()
                        .HasForeignKey("CategoryId");

                    b.HasOne("Deal.Models.Group")
                        .WithMany()
                        .HasForeignKey("GroupId");

                    b.HasOne("Deal.Models.Offer")
                        .WithMany()
                        .HasForeignKey("OfferId");

                    b.HasOne("Deal.Models.Identity.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Deal.Models.UserGroup", b =>
                {
                    b.HasOne("Deal.Models.Group")
                        .WithMany()
                        .HasForeignKey("GroupId");

                    b.HasOne("Deal.Models.Identity.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Deal.Models.Identity.ApplicationRole")
                        .WithMany()
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("Deal.Models.Identity.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("Deal.Models.Identity.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Deal.Models.Identity.ApplicationRole")
                        .WithMany()
                        .HasForeignKey("RoleId");

                    b.HasOne("Deal.Models.Identity.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });
        }
    }
}
