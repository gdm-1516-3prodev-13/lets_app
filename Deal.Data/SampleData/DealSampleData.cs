using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Deal.Data;
using Deal.Models;
using Deal.Models.Identity;

namespace Deal.Data.SampleData
{
    public class DealSampleData
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly DealDbContext _context;
        
        public DealSampleData(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager, DealDbContext context)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
        }
        
        public async void InitializeData() 
        {
            await CreateRoles();
            await CreateUsers();
            CreateGroups();
            CreateCategories();
			CreateRequests();
            CreateComments();
        }
        
        private async Task<bool> CreateRoles()
        {
            if(_context.Roles == null || _context.Roles.Count() == 0)
            {
                var role = new ApplicationRole 
                { 
                    Name = "Administrator", 
                    Description = "Administrator: administration of all the content."
                };
                var result = await _roleManager.CreateAsync(role);
                
                if(!result.Succeeded)
                    return false;
                    
                role = new ApplicationRole 
                { 
                    Name = "Moderator", 
                    Description = "Moderator: administration of all the content within a certain group."
                };
                result = await _roleManager.CreateAsync(role);
                
                if(!result.Succeeded)
                    return false;
            }
            return true;
        }
        
        private async Task<bool> CreateUsers()
        {
            if(_context.Users == null || _context.Users.Count() == 0)
            {
                var username = "JohnDoe";
                var email = username + "@email.com";
                var user = new ApplicationUser
                {
                    UserName = username,
                    Email = email
                };
                var result = await _userManager.CreateAsync(user, "password");
                
                if(!result.Succeeded)
                    return false;
                
                var roleResult = await _userManager.AddToRoleAsync(_context.Users.Where(m => m.UserName == username).FirstOrDefault(), "Administrator");
                
                if(!roleResult.Succeeded)
                    return false;
                    
                var profile = new Profile
                {
                    UserId = _context.Users.Where(m => m.UserName == username).FirstOrDefault().Id,
                    FirstName = "John",
                    LastName = "Doe",
                    Description = "Administrator of Deal",
                    Location = "Gent, Belgium",
                    Points = 50
                };
                
                _context.Profiles.Add(profile);
                var resultProfile = await _context.SaveChangesAsync();
                if(resultProfile == 0)
                    return false;
            }
            return true;
        }
        
        private void CreateGroups()
        {
            if(_context.Groups == null || _context.Groups.Count() == 0)
            {
                var usr = _context.Users.FirstOrDefault();
                var grp = new Group();
                
                var userList = new List<UserGroup>();
                var usrgrp = new UserGroup
                {
                    UserId = usr.Id,
                    GroupId = grp.Id
                };
                userList.Add(usrgrp);
                
                grp.Name = "SampleGroup A";
                grp.Description = "This is a sample group.";
                grp.Users = userList;
                grp.UserId = usr.Id;
                
                _context.Groups.Add(grp);
                _context.UserGroups.Add(usrgrp);
                
                _context.SaveChanges();
            }
        }
        
        private void CreateCategories()
        {
            if(_context.Categories == null || _context.Categories.Count() == 0)
            {
                var user = _context.Users.FirstOrDefault();
                var cat1 = new Category
                {
                    Name = "SampleCategory A",
                    Description = "This is a sample category.",
                    User = user
                };
                _context.Categories.Add(cat1);
                var cat2 = new Category
                {
                    Name = "SampleCategory B",
                    Description = "This is a sample category.",
                    User = user
                };
                _context.Categories.Add(cat2);
                
                _context.SaveChanges();
            }
        }
        
        private void CreateRequests()
        {
            if(_context.Requests == null || _context.Requests.Count() == 0)
            {
                var request = new Request
                {
                    Title = "Request A",
                    Description = "This is a sample request.",
					Bonus = TypeBonus.Medium,
					EstimatedTime = 60,
                    IsCompleted = false,
                    Category = _context.Categories.FirstOrDefault(),
                    Group = _context.Groups.FirstOrDefault(),
                    User = _context.Users.FirstOrDefault()
                };
                _context.Requests.Add(request);

                _context.SaveChanges();
            }
            CreateOffers();
        }
        
        private void CreateOffers()
        {
            if(_context.Offers == null || _context.Offers.Count() == 0)
            {
                var user = _context.Users.FirstOrDefault();
                var offer = new Offer
                {
                    Title = "Offer A",
                    Description = "This is a sample offer.",
                    Category = _context.Categories.FirstOrDefault(),
                    Group = _context.Groups.FirstOrDefault(),
                    User = user
                };
                _context.Offers.Add(offer);
                
                _context.SaveChanges();
                
                CreateSubRequests();
                CreateSubOffers();
            }
        }
        
        private void CreateSubRequests()
        {
            if(_context.Offers != null || _context.Offers.Count() > 0)
            {
                var offer = _context.Offers.Where(m => m.Title == "Offer A").FirstOrDefault();
                if(offer != null)
                {
                    var request = new Request
                    {
                        Title = "SubRequest A",
                        Description = "This is a sample subrequest.",
                        Bonus = TypeBonus.Medium,
                        EstimatedTime = 60,
                        IsCompleted = false,
                        Category = offer.Category,
                        Group = offer.Group,
                        User = _context.Users.FirstOrDefault(),
                        ParentOffer = offer
                    };
                    _context.Requests.Add(request);

                    _context.SaveChanges();
                }
            }
        }
        
        private void CreateSubOffers()
        {
            if(_context.Requests != null || _context.Requests.Count() != 0)
            {
                var request = _context.Requests.Where(m => m.Title == "Request A").FirstOrDefault();
                if(request != null)
                {
                    var offer = new Offer
                    {
                        Title = "SubOffer A",
                        Description = "This is a sample suboffer.",
                        Category = request.Category,
                        Group = request.Group,
                        User = _context.Users.FirstOrDefault(),
                        ParentRequest = request,
                    };
                    _context.Offers.Add(offer);
                    
                    _context.SaveChanges();
                }
            }
        }
        
        private void CreateComments()
        {
            if(_context.Comments == null || _context.Comments.Count() == 0)
            {
                _context.Comments.Add(new Comment()
                {
                   Content = "This is a sample comment.",
                   Request = _context.Requests.FirstOrDefault(),
                   User = _context.Users.FirstOrDefault() 
                });
                
                _context.SaveChanges();
            }
        }
    }
}
