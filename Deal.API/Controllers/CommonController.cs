using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Identity;
using Deal.Data;
using Deal.Models;
using Deal.Models.Identity;

namespace Deal.API.Controllers
{    
    public abstract class CommonController : Controller
    {
        [FromServices]
        public DealDbContext _dealContext { get; set; }
        public UserManager<ApplicationUser> _userManager { get; set; }
    }
}
