using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Deal.Data;
using Deal.Models;

namespace Deal.API.Controllers
{
    [Route("api/[controller]")]
    public class GroupController : CommonController
    {
        // GET: api/group
        [HttpGet(Name = "GetGroups")]
        public IEnumerable<Group> GetGroups()
        {
            return _dealContext.Groups.AsEnumerable();
        }
        
        // GET: api/group/1
        [HttpGet("{groupId:int}", Name = "GetGroupById")]
        public IActionResult GetGroupById(int groupId)
        {
            var model = _dealContext.Groups.FirstOrDefault(c => c.Id == groupId);
            if (model == null)
            {
                return HttpNotFound();
            }
            
            return new ObjectResult(model);
        }
        
        // POST : api/group
        [HttpPost(Name = "AddGroup")]
        public IActionResult AddGroup([FromBody]Group group)
        {
            if(!ModelState.IsValid)
            {
                HttpContext.Response.StatusCode = 400;
                return new ObjectResult(
                    new GroupStatus {
                        Id = StatusTypes.ModelInvalid,
                        Description = "Group model is invalid. " + ModelState.ToString()
                    }
                );
            }
            else
            {
                _dealContext.Groups.Add(group);
                var addedGroup = (_dealContext.SaveChanges() > 0) ? group : null;
                
                if (addedGroup != null)
                {
                    string url = Url.RouteUrl("GetGroupById", new { groupId = group.Id }, Request.Scheme, Request.Host.ToUriComponent());
                    
                    HttpContext.Response.StatusCode = 201;
                    HttpContext.Response.Headers["Location"] = url;
                    return new ObjectResult(addedGroup);
                }
                else
                {
                    HttpContext.Response.StatusCode = 400;
                    return new ObjectResult(new GroupStatus { Id = StatusTypes.AddedFailure, Description = "Failed to save group" });
                }
            }
        }
        
        // PUT : api/group/1
        [HttpPut("{groupId:int}", Name = "UpdateGroup")]
        public IActionResult UpdateGroup(int groupId, [FromBody]Group group)
        {
            var originalGroup = _dealContext.Groups.FirstOrDefault(c => c.Id == groupId);
            
            if (originalGroup == null)
            {
                return HttpNotFound();
            }
            
            originalGroup.Name = group.Name;
            originalGroup.Description = group.Description;
            originalGroup.User = group.User;
            
            if(_dealContext.SaveChanges() > 0)
            {
                return new HttpStatusCodeResult(204);
            }
            else
            {
                return HttpNotFound();
            }
        }
        
        // DELETE : api/group/1
        [HttpDelete("{groupId:int}", Name = "DeleteGroup")]
        public IActionResult DeleteGroup(int groupId)
        {
            var group = _dealContext.Groups.FirstOrDefault(c => c.Id == groupId);
            
            if (group == null)
            {
                return new HttpNotFoundResult();
            }
            
            _dealContext.Groups.Remove(group);
            _dealContext.Entry(group).State = EntityState.Deleted;
            
            if (_dealContext.SaveChanges() > 0)
            {
                return new HttpStatusCodeResult(204);
            }
            else
            {
                return HttpNotFound();
            }
        }
    }
}
