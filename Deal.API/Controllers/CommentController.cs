using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Deal.Data;
using Deal.Models;

namespace Deal.API.Controllers
{
    [Route("api/[controller]")]
    public class CommentController : CommonController
    {
        // GET: api/comment
        [HttpGet(Name = "GetComments")]
        public IEnumerable<Comment> GetComments()
        {
            return _dealContext.Comments.AsEnumerable();
        }
        
        // GET: api/comment/1
        [HttpGet("{commentId:int}", Name = "GetCommentById")]
        public IActionResult GetCommentById(int commentId)
        {
            var model = _dealContext.Comments.FirstOrDefault(c => c.Id == commentId);
            if (model == null)
            {
                return HttpNotFound();
            }
            
            return new ObjectResult(model);
        }
        
        // POST : api/comment
        [HttpPost(Name = "AddComment")]
        public IActionResult AddComment([FromBody]Comment comment)
        {
            if(!ModelState.IsValid)
            {
                HttpContext.Response.StatusCode = 400;
                return new ObjectResult(
                    new GroupStatus {
                        Id = StatusTypes.ModelInvalid,
                        Description = "Comment model is invalid. " + ModelState.ToString()
                    }
                );
            }
            else
            {
                _dealContext.Comments.Add(comment);
                var addedComment = (_dealContext.SaveChanges() > 0) ? comment : null;
                
                if (addedComment != null)
                {
                    string url = Url.RouteUrl("GetCommentById", new { commentId = comment.Id }, Request.Scheme, Request.Host.ToUriComponent());
                    
                    HttpContext.Response.StatusCode = 201;
                    HttpContext.Response.Headers["Location"] = url;
                    return new ObjectResult(addedComment);
                }
                else
                {
                    HttpContext.Response.StatusCode = 400;
                    return new ObjectResult(new GroupStatus { Id = StatusTypes.AddedFailure, Description = "Failed to save comment" });
                }
            }
        }
        
        // PUT : api/comment/1
        [HttpPut("{commentId:int}", Name = "UpdateComment")]
        public IActionResult UpdateComment(int commentId, [FromBody]Comment comment)
        {
            var originalComment = _dealContext.Comments.FirstOrDefault(c => c.Id == commentId);
            
            if (originalComment == null)
            {
                return HttpNotFound();
            }
            
            originalComment.Content = comment.Content;
            originalComment.User = comment.User;
            originalComment.Request = comment.Request;
            originalComment.Offer = comment.Offer;
            
            if(_dealContext.SaveChanges() > 0)
            {
                return new HttpStatusCodeResult(204);
            }
            else
            {
                return HttpNotFound();
            }
        }
        
        // DELETE : api/comment/1
        [HttpDelete("{commentId:int}", Name = "DeleteComment")]
        public IActionResult DeleteComment(int commentId)
        {
            var comment = _dealContext.Comments.FirstOrDefault(c => c.Id == commentId);
            
            if (comment == null)
            {
                return new HttpNotFoundResult();
            }
            
            _dealContext.Comments.Remove(comment);
            _dealContext.Entry(comment).State = EntityState.Deleted;
            
            if (_dealContext.SaveChanges() > 0)
            {
                return new HttpStatusCodeResult(204);
            }
            else
            {
                return HttpNotFound();
            }
        }
    }
}
