using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Deal.Data;
using Deal.Models;
using Deal.Models.Identity;

namespace Deal.API.Controllers
{
    [Route("api/[controller]")]
    public class UserController : CommonController
    {
        // GET: api/user
        [HttpGet(Name = "GetUsers")]
        public IEnumerable<ApplicationUser> GetUsers()
        {
            return _dealContext.Users.Include(c => c.Profile).AsEnumerable();
        }
    }
}
