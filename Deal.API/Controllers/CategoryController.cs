using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Deal.Data;
using Deal.Models;

namespace Deal.API.Controllers
{
    [Route("api/[controller]")]
    public class CategoryController : CommonController
    {
        // GET: api/category
        [HttpGet(Name = "GetCategories")]
        public IEnumerable<Category> GetCategories()
        {
            return _dealContext.Categories.AsEnumerable();
        }
        
        // GET: api/category/1
        [HttpGet("{categoryId:int}", Name = "GetCategoryById")]
        public IActionResult GetCategoryById(int categoryId)
        {
            var model = _dealContext.Categories.FirstOrDefault(c => c.Id == categoryId);
            if (model == null)
            {
                return HttpNotFound();
            }
            
            return new ObjectResult(model);
        }
        
        // POST : api/category
        [HttpPost(Name = "AddCategory")]
        public IActionResult AddCategory([FromBody]Category category)
        {
            if(!ModelState.IsValid)
            {
                HttpContext.Response.StatusCode = 400;
                return new ObjectResult(
                    new GroupStatus {
                        Id = StatusTypes.ModelInvalid,
                        Description = "Category model is invalid. " + ModelState.ToString()
                    }
                );
            }
            else
            {
                _dealContext.Categories.Add(category);
                var addedCategory = (_dealContext.SaveChanges() > 0) ? category : null;
                
                if (addedCategory != null)
                {
                    string url = Url.RouteUrl("GetCategoryById", new { categoryId = category.Id }, Request.Scheme, Request.Host.ToUriComponent());
                    
                    HttpContext.Response.StatusCode = 201;
                    HttpContext.Response.Headers["Location"] = url;
                    return new ObjectResult(addedCategory);
                }
                else
                {
                    HttpContext.Response.StatusCode = 400;
                    return new ObjectResult(new GroupStatus { Id = StatusTypes.AddedFailure, Description = "Failed to save category" });
                }
            }
        }
        
        // PUT : api/category/1
        [HttpPut("{categoryId:int}", Name = "UpdateCategory")]
        public IActionResult UpdateCategory(int categoryId, [FromBody]Category category)
        {
            var originalCategory = _dealContext.Categories.FirstOrDefault(c => c.Id == categoryId);
            
            if (originalCategory == null)
            {
                return HttpNotFound();
            }
            
            originalCategory.Name = category.Name;
            originalCategory.Description = category.Description;
            originalCategory.User = category.User;
            
            if(_dealContext.SaveChanges() > 0)
            {
                return new HttpStatusCodeResult(204);
            }
            else
            {
                return HttpNotFound();
            }
        }
        
        // DELETE : api/category/1
        [HttpDelete("{categoryId:int}", Name = "DeleteCategory")]
        public IActionResult DeleteCategory(int categoryId)
        {
            var category = _dealContext.Categories.FirstOrDefault(c => c.Id == categoryId);
            
            if (category == null)
            {
                return new HttpNotFoundResult();
            }
            
            _dealContext.Categories.Remove(category);
            _dealContext.Entry(category).State = EntityState.Deleted;
            
            if (_dealContext.SaveChanges() > 0)
            {
                return new HttpStatusCodeResult(204);
            }
            else
            {
                return HttpNotFound();
            }
        }
    }
}
