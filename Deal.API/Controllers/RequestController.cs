using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Deal.Data;
using Deal.Models;

namespace Deal.API.Controllers
{
    [Route("api/[controller]")]
    public class RequestController : CommonController
    {
        // GET: api/request
        [HttpGet(Name = "GetRequests")]
        public IEnumerable<Request> GetRequests()
        {
            return _dealContext.Requests.AsEnumerable();
        }
        
        // GET: api/request/1
        [HttpGet("{requestId:int}", Name = "GetRequestById")]
        public IActionResult GetRequestById(int requestId)
        {
            var model = _dealContext.Requests.FirstOrDefault(c => c.Id == requestId);
            if (model == null)
            {
                return HttpNotFound();
            }
            
            return new ObjectResult(model);
        }
        
        // POST : api/request
        [HttpPost(Name = "AddRequest")]
        public IActionResult AddRequest([FromBody]Request request)
        {
            if(!ModelState.IsValid)
            {
                HttpContext.Response.StatusCode = 400;
                return new ObjectResult(
                    new GroupStatus {
                        Id = StatusTypes.ModelInvalid,
                        Description = "Request model is invalid. " + ModelState.ToString()
                    }
                );
            }
            else
            {
                _dealContext.Requests.Add(request);
                var addedRequest = (_dealContext.SaveChanges() > 0) ? request : null;
                
                if (addedRequest != null)
                {
                    string url = Url.RouteUrl("GetRequestById", new { requestId = request.Id }, Request.Scheme, Request.Host.ToUriComponent());
                    
                    HttpContext.Response.StatusCode = 201;
                    HttpContext.Response.Headers["Location"] = url;
                    return new ObjectResult(addedRequest);
                }
                else
                {
                    HttpContext.Response.StatusCode = 400;
                    return new ObjectResult(new GroupStatus { Id = StatusTypes.AddedFailure, Description = "Failed to save request" });
                }
            }
        }
        
        // PUT : api/request/1
        [HttpPut("{requestId:int}", Name = "UpdateRequest")]
        public IActionResult UpdateRequest(int requestId, [FromBody]Request request)
        {
            var originalRequest = _dealContext.Requests.FirstOrDefault(c => c.Id == requestId);
            
            if (originalRequest == null)
            {
                return HttpNotFound();
            }
            
            originalRequest.Title = request.Title;
            originalRequest.Description = request.Description;
            originalRequest.Bonus = request.Bonus;
            originalRequest.EstimatedTime = request.EstimatedTime;
            originalRequest.IsCompleted = request.IsCompleted;
            originalRequest.User = request.User;
            originalRequest.Group = request.Group;
            originalRequest.Category = request.Category;
            
            if(_dealContext.SaveChanges() > 0)
            {
                return new HttpStatusCodeResult(204);
            }
            else
            {
                return HttpNotFound();
            }
        }
        
        // DELETE : api/request/1
        [HttpDelete("{requestId:int}", Name = "DeleteRequest")]
        public IActionResult DeleteRequest(int requestId)
        {
            var request = _dealContext.Requests.FirstOrDefault(c => c.Id == requestId);
            
            if (request == null)
            {
                return new HttpNotFoundResult();
            }
            
            _dealContext.Requests.Remove(request);
            _dealContext.Entry(request).State = EntityState.Deleted;
            
            if (_dealContext.SaveChanges() > 0)
            {
                return new HttpStatusCodeResult(204);
            }
            else
            {
                return HttpNotFound();
            }
        }
    }
}
