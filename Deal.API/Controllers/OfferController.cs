using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Deal.Data;
using Deal.Models;

namespace Deal.API.Controllers
{
    [Route("api/[controller]")]
    public class OfferController : CommonController
    {
        // GET: api/offer
        [HttpGet(Name = "GetOffers")]
        public IEnumerable<Offer> GetOffers()
        {
            return _dealContext.Offers.AsEnumerable();
        }
        
        // GET: api/offer/1
        [HttpGet("{offerId:int}", Name = "GetOfferById")]
        public IActionResult GetOfferById(int offerId)
        {
            var model = _dealContext.Offers.FirstOrDefault(c => c.Id == offerId);
            if (model == null)
            {
                return HttpNotFound();
            }
            
            return new ObjectResult(model);
        }
        
        // POST : api/offer
        [HttpPost(Name = "AddOffer")]
        public IActionResult AddOffer([FromBody]Offer offer)
        {
            if(!ModelState.IsValid)
            {
                HttpContext.Response.StatusCode = 400;
                return new ObjectResult(
                    new GroupStatus {
                        Id = StatusTypes.ModelInvalid,
                        Description = "Offer model is invalid. " + ModelState.ToString()
                    }
                );
            }
            else
            {
                _dealContext.Offers.Add(offer);
                var addedOffer = (_dealContext.SaveChanges() > 0) ? offer : null;
                
                if (addedOffer != null)
                {
                    string url = Url.RouteUrl("GetOfferById", new { offerId = offer.Id }, Request.Scheme, Request.Host.ToUriComponent());
                    
                    HttpContext.Response.StatusCode = 201;
                    HttpContext.Response.Headers["Location"] = url;
                    return new ObjectResult(addedOffer);
                }
                else
                {
                    HttpContext.Response.StatusCode = 400;
                    return new ObjectResult(new GroupStatus { Id = StatusTypes.AddedFailure, Description = "Failed to save offer" });
                }
            }
        }
        
        // PUT : api/offer/1
        [HttpPut("{offerId:int}", Name = "UpdateOffer")]
        public IActionResult UpdateOffer(int offerId, [FromBody]Offer offer)
        {
            var originalOffer = _dealContext.Offers.FirstOrDefault(c => c.Id == offerId);
            
            if (originalOffer == null)
            {
                return HttpNotFound();
            }
            
            originalOffer.Title = offer.Title;
            originalOffer.Description = offer.Description;
            originalOffer.IsCompleted = offer.IsCompleted;
            originalOffer.ParentRequest = offer.ParentRequest;
            originalOffer.User = offer.User;
            originalOffer.Group = offer.Group;
            originalOffer.Category = offer.Category;
            
            if(_dealContext.SaveChanges() > 0)
            {
                return new HttpStatusCodeResult(204);
            }
            else
            {
                return HttpNotFound();
            }
        }
        
        // DELETE : api/offer/1
        [HttpDelete("{offerId:int}", Name = "DeleteOffer")]
        public IActionResult DeleteOffer(int offerId)
        {
            var offer = _dealContext.Offers.FirstOrDefault(c => c.Id == offerId);
            
            if (offer == null)
            {
                return new HttpNotFoundResult();
            }
            
            _dealContext.Offers.Remove(offer);
            _dealContext.Entry(offer).State = EntityState.Deleted;
            
            if (_dealContext.SaveChanges() > 0)
            {
                return new HttpStatusCodeResult(204);
            }
            else
            {
                return HttpNotFound();
            }
        }
    }
}
