'use strict';
var gulp  = require("gulp"),
  gutil   = require("gulp-util"),
  rimraf  = require("rimraf"),
  concat  = require("gulp-concat"),
  cssmin  = require("gulp-cssmin"),
  uglify  = require("gulp-uglify"),
  sass    = require("gulp-sass"),
  rename  = require('gulp-rename'),
  watch   = require('gulp-watch'),
  project = require("./project.json"),
  livereload = require('gulp-livereload');

// Define paths & files.
var paths = {
  webroot: "./" + project.webroot + "/"
};
paths.js = paths.webroot + "js/";
paths.css = paths.webroot + "css/";
paths.sass = paths.webroot + "scss/";

var files = {
  css: paths.webroot + "css/**/*.css",
  minCss: paths.webroot + "css/**/*.min.css",
  sass: paths.webroot + "scss/*.scss",
  js: paths.webroot + "js/**/*.js"
};

// Default task when calling Gulp.
gulp.task('default', ['build', 'watch']);

// Build the application.
gulp.task('build', ['min'], function(done) {
   console.log(gutil.colors.blue.bgYellow.bold(' App built. ')); 
});

// Compile .scss files.
gulp.task('sass', function(done) {
  gulp.src(files.sass)
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest(paths.css))
    .pipe(livereload())
    .on('end', done);
});

// Watch and compile changed .scss files.
gulp.task('watch', function() {
  livereload.listen();
  gulp.watch(paths.sass + "**/*.scss", ['sass'])
    .on('change', function(event){
      console.log('Event Type: ' + event.type);
      console.log('Event Type: ' + event.path);
    });
});

// Minify files.
gulp.task("min", ["min:css"]);

// Minify compiled CSS files.
gulp.task("min:css", ['sass'], function() {
    gulp.src([files.css, "!" + files.minCss])
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.css))
});
