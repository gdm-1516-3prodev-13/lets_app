$(function() {
	var source = $('#dropdown-source'),
	target = $('.dropdown-target');
	
	if(source) {
		
		if(source.val() != "")
		{
			target.val(null);
			target.find('option:selected').text("Inherited");
			target.attr('disabled', 'disabled');
		} else {
			//target.find('option:selected').text("-- Select an option --");
			target.attr('disabled', false);
		}
		
		
		source.change(function(){
			if($(this).val())
			{
				target.val(null);
				target.find('option:selected').text("Inherited");
				target.attr('disabled', 'disabled');
			} else {
				target.find('option:selected').text("-- Select an option --");
				target.attr('disabled', false);
			}
		});
	}
});