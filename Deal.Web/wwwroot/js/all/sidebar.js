﻿//NAMELESS FUNCTION - AUTO EXECUTED WHEN DOCUMENT IS LOADED
(function () {

    //PUSH SIDEBAR
    var sidebar = $('.sidebar-backoffice'),
        main = $('#main'),
        sidebarToggle = $('.sidebar-toggle');

    if (Modernizr.csstransforms3d) {
        sidebarToggle.on('click', function (e) {
            e.preventDefault();
            toggleSidebar();
            return false;
        });
    } else {

    }

    //TOGGLE SIDEBAR AND MAIN
    function toggleSidebar() {
        main.toggleClass('main-push');
        sidebar.toggleClass('sidebar-push-left sidebar-push-open')
    }

    //UI
    function refreshUI() {
        if ($(window).width() >= 800) {
            sidebar.removeClass('sidebar-push-open');
            sidebar.removeClass('sidebar-push-left');
            main.removeClass('main-push');
            main.addClass('main-full');
            sidebarToggle.addClass('hidden');
        } else {
            sidebarToggle.removeClass('hidden');
            sidebar.addClass('sidebar-push-left');
            main.removeClass('main-full');
        }
    }

    //Resize Window
    $(window).on('resize', function (ev) {
        refreshUI();
    });

    refreshUI();
})();