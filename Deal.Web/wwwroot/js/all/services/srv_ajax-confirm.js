/*
 * Programmed by Philippe De Pauw -Waterschoot
 * Version: 1.0
 * Last updated: 15-10-2015
 * Education license: only use in educationel institutions
 * 
 *  class="confirm-link"
    data-id="@library.Id" 
    data-name="@library.Name"
    data-cat = "library"
    data-action="delete"  
    data-update-target="#libraries-list"
    data-update-href=""
 */
var ConfirmService = (function ($) {

    var _dataId, _dataName, _dataCategory, _dataAction, _dataUpdateTarget, _dataUpdateHref, _dataRedirectHref;
    var _message;
    var _href;
    


    var _confirmLinkClass, _confirmDialogId;
    var self;

    ConfirmService.prototype.constructor = ConfirmService;
    function ConfirmService(linkClass, dialogId) {
        _confirmLinkClass = linkClass;
        _confirmDialogId = dialogId;
        self = this;
    }

    ConfirmService.prototype.registerModalHandlers = function () {
        $(_confirmDialogId).find('.btn-confirm').click(function (ev) {
            ev.preventDefault();

            $.ajaxPrefilter(
               function (options, localOptions, jqXHR) {
                   if (options.type !== "GET") {
                       var token = GetAntiForgeryToken(_dataUpdateTarget);
                       if (token !== null) {
                           options.data = "X-Requested-With=XMLHttpRequest" + (options.data === "") ? "" : "&" + options.data;
                           options.data = options.data + "&" + token.name + '=' + token.value;
                       }
                   }
               }
           );

            $.ajax({
                url: _href,
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    if (data.state === 1) {

                        if (window.AlertService) {
                            window.AlertService.showAlert('Success', data.message);
                        }

                        var itemContainer = $('[data-id="' + _dataCategory + '-' + data.id + '"]');

                        $(itemContainer).animate({ opacity: 0.4 }, 680, function () {
                            
                            if(_dataAction == 'delete') {
                                (this).remove();
                            }
                            
                            if(_dataRedirectHref != null) {
                                window.location.replace(_dataRedirectHref);
                            } else {
                                $.get(_dataUpdateHref, function (data) {
                                    $(_dataUpdateTarget).html(data);

                                    self.registerClickHandlers();
                                });
                            }
                        });

                    } else {
                        if (window.AlertManager) {
                            window.AlertManager.showAlert('Error', data.message);
                        }
                    }
                }
            });

            $('#confirm-dialog').modal('hide');

            return false;
        });
    }

    ConfirmService.prototype.registerClickHandlers = function () {
        $(_confirmLinkClass).click(function (ev) {
            ev.preventDefault();

            _dataId = $(this).data('id');
            _dataName = $(this).data('name');
            _dataCategory = $(this).data('cat');
            _dataAction = $(this).data('action')
            _dataUpdateTarget = $(this).data('update-target');
            _dataUpdateHref = $(this).data('update-href');
            _dataRedirectHref = $(this).data('redirect-href');
            _href = $(this)[0].href;

            switch (_dataAction) {
                case 'delete':
                    _message = "Are you sure you want to <strong>delete</strong> " + _dataCategory + ": " + _dataName + "?";
                    break;
                case 'lock':
                    _message = "Are you sure you want to <strong>lock</strong> " + _dataCategory + ": " + _dataName + "?";
                    break;
                case 'unlock':
                    _message = "Are you sure you want to <strong>unlock</strong> " + _dataCategory + ": " + _dataName + "?";
                    break;
                case 'softdelete':
                    _message = "Are you sure you want to <strong>soft-delete</strong> " + _dataCategory + ": " + _dataName + "?";
                    break;
                case 'softundelete':
                    _message = "Are you sure you want to <strong>soft-undelete</strong> " + _dataCategory + ": " + _dataName + "?";
                    break;
                case 'enabletwofactor':
                    _message = "Are you sure you want to <strong>enable</strong> the Two Factor Auth for " + _dataCategory + ": " + _dataName + "?";
                    break;
                case 'disabletwofactor':
                    _message = "Are you sure you want to <strong>disable</strong> the Two Factor Auth for " + _dataCategory + ": " + _dataName + "?";
                    break;
                case 'complete':
                    _message = "Are you sure you want to <strong>complete</strong> " + _dataCategory + ": " + _dataName + "?";
                    break;
                case 'uncomplete':
                    _message = "Are you sure you want to <strong>uncomplete</strong> " + _dataCategory + ": " + _dataName + "?";
                    break;
                case 'join':
                    _message = "Are you sure you want to <strong>join</strong> " + _dataCategory + ": " + _dataName + "?";
                    break;
                case 'leave':
                    _message = "Are you sure you want to <strong>leave</strong> " + _dataCategory + ": " + _dataName + "?";
                    break;
                case 'add':
                    _message = "Are you sure you want to <strong>add</strong> " + _dataCategory + ": " + _dataName + "?";
                    break;
                case 'remove':
                    _message = "Are you sure you want to <strong>remove</strong> " + _dataCategory + ": " + _dataName + "?";
                    break;
            }

            $(_confirmDialogId).find('.modal-body').html(_message);
            $(_confirmDialogId).modal('show');

            return false;
        });        
    };

    return ConfirmService;
})(jQuery);