using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.AspNet.Authorization;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Storage;
using Deal.Models.Identity;
using Deal.Models.Identity.ViewModels;

namespace Deal.Web.Areas.Backoffice.Controllers
{
    [Area("Backoffice")]
    [Authorize(Roles = "Administrator")]
    public class RoleController : CommonController
    {
        [HttpGet]
        public IActionResult Index()
        {
            var model = _dealContext.Roles.AsEnumerable().OrderByDescending(m => m.CreatedAt);
            
            if(this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                return PartialView("_ListPartial", model);
            }
            
            return View(model);
        }
        
        [HttpGet]
        public ActionResult Create()
        {
            var model = new AdminRoleViewModel();
            
            return View(model);
        }
        
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> Create(AdminRoleViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Role is not valid");

                var role = new ApplicationRole { Name = model.Name, Description = model.Description};
                var result = await _applicationRoleManager.CreateAsync(role);

                if (!result.Succeeded)
                {
                    throw new Exception("Can't create the role!");
                }
                
                var msg = CreateMessage(ControllerActionType.Create, "role", model.Name);
                return RedirectToAction("Index");
                
            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Create, "role", model.Name, ex);
                return View(model);
            }
        }
        
        [HttpGet]
        public IActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(400);
            }
            
            var model = _dealContext.Roles.FirstOrDefault(m => m.Id == id);
            if(model == null)
            {
                return RedirectToAction("Index");
            }
            
            var viewModel = new AdminRoleViewModel
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description
            };
                        
            return View(viewModel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(AdminRoleViewModel model)
        {
            AdminRoleViewModel viewModel = null;
            try 
            {
                if(!ModelState.IsValid)
                    throw new Exception("The Role model is not valid!");
                    
                var originalModel = _dealContext.Roles.FirstOrDefault(m => m.Id == model.Id);
                
                if(originalModel == null)
                    throw new Exception("The existing Role: " + model.Name + " doesn't exist anymore!");
                    
                originalModel.Name = model.Name;
                originalModel.Description = model.Description;
                
                
                _dealContext.Roles.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Role model could not be saved!");
                }
                
                return RedirectToAction("Index");
            
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                
                viewModel = new AdminRoleViewModel
                {
                    Id = model.Id,
                    Name = model.Name,
                    Description = model.Description
                };    
            }
            return View(viewModel);
        }
        
        [HttpPost]
        public IActionResult Delete(string id)
        {
            try
            {
                var originalModel = _dealContext.Roles.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Role with id: " + id + " doesn't exist anymore!");

                _dealContext.Roles.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Deleted;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Role model could not be saved!");
                } 

                var msg = CreateMessage(ControllerActionType.Delete, "role", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Role");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "role", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Role");
                }
            }
        }
        
        [HttpPost]
        public IActionResult SoftDelete(string id)
        {
            try
            {
                var originalModel = _dealContext.Roles.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Role with id: " + id + " doesn't exist anymore!");

                originalModel.DeletedAt = DateTime.Now;
                _dealContext.Roles.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Role model could not be soft deleted!");
                } 

                var msg = CreateMessage(ControllerActionType.SoftDelete, "role", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Role");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.SoftDelete, "role", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Role");
                }
            }
        }
        
        [HttpPost]
        public IActionResult SoftUnDelete(string id)
        {
            try
            {
                var originalModel = _dealContext.Roles.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Role with id: " + id + " doesn't exist anymore!");

                originalModel.DeletedAt = null;
                _dealContext.Roles.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Role model could not be soft undeleted!");
                } 

                var msg = CreateMessage(ControllerActionType.SoftUnDelete, "role", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Role");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.SoftUnDelete, "role", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Role");
                }
            }
        }
        
        [HttpGet]
        public IActionResult Users(string id)
        {
            var role = _dealContext.Roles
                .FirstOrDefault(m => m.Id == id);
                
            var users = _dealContext.Users
                .Where(m => m.DeletedAt == null)
                .Include(m => m.Roles)
                .AsEnumerable();
            var usersInRole = _dealContext.Users
                .Where(m => m.Roles.Any(r => r.RoleId == id))
                .AsEnumerable();
            var usersNotInRole = users.Except(usersInRole).AsEnumerable();
            
            var model = new AdminUserRoleViewModel
            {
                Role = role,
                UsersInRole = usersInRole,
                UsersNotInRole = usersNotInRole
            };
            
            if(this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                return PartialView("_UserListPartial", model);
            }
            
            return View(model);
        }
        
        [HttpPost]
        public async Task<IActionResult> AddUser(string roleId, string userId)
        {
            try
            {
                var role = _dealContext.Roles.FirstOrDefault(m => m.Id == roleId);
                var user = _dealContext.Users.FirstOrDefault(m => m.Id == userId);
                
                if(role == null || user == null)
                    throw new Exception("Couldn't find one or more models");
                    
                    
                var result = await _applicationUserManager.AddToRoleAsync(user, role.Name); 
                
                if(!result.Succeeded)
                    throw new Exception("Couldn't add user to the role");
                
                
                var msg = CreateMessage(ControllerActionType.Edit, "role", roleId);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = roleId, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Role");
                }
            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Edit, "role", roleId, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = roleId, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Role");
                }
            }
        }
        
        [HttpPost]
        public async Task<IActionResult> RemoveUser(string roleId, string userId)
        {
            try
            {
                var role = _dealContext.Roles.FirstOrDefault(m => m.Id == roleId);
                var user = _dealContext.Users.FirstOrDefault(m => m.Id == userId);
                
                if(role == null || user == null)
                    throw new Exception("Couldn't find one or more models");
                    
                if(User.GetUserId() == user.Id)
                    throw new Exception("You can't remove yourself from this role!");
                    
                    
                var result = await _applicationUserManager.RemoveFromRoleAsync(user, role.Name); 
                
                if(!result.Succeeded)
                    throw new Exception("Couldn't remove user from the role");
                
                
                var msg = CreateMessage(ControllerActionType.Edit, "role", roleId);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = roleId, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Role");
                }
            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Edit, "role", roleId, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = roleId, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Role");
                }
            }
        }
    }
}
