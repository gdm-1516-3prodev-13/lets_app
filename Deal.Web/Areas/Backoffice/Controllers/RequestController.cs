using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.AspNet.Authorization;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Storage;
using Deal.Models;
using Deal.Models.ViewModels;

namespace Deal.Web.Areas.Backoffice.Controllers
{
    [Area("Backoffice")]
    [Authorize(Roles = "Administrator")]
    public class RequestController : CommonController
    {
        [HttpGet]
        public IActionResult Index()
        {
            var model = _dealContext.Requests.AsEnumerable().OrderByDescending(m => m.CreatedAt);
            
            if(this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                return PartialView("_ListPartial", model);
            }
            
            return View(model);
        }
        
        [HttpGet]
        public IActionResult Create()
        {
            var viewmodel = new RequestViewModel
            {
              Request = new Request(),
              Groups = _dealContext.Groups.AsEnumerable().OrderBy(m => m.Name),
              Users = _dealContext.Users.Include(l => l.Profile).AsEnumerable().OrderByDescending(m => m.CreatedAt),
              Categories = _dealContext.Categories.AsEnumerable().OrderBy(m => m.Name)
            };
            
            return View(viewmodel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(RequestViewModel viewmodel)
        {
            try
            {
                if(!ModelState.IsValid)
                    throw new Exception("The RequestViewModel is not valid!");
                        
                var request = new Request
                {
                    Title = viewmodel.Request.Title,
                    Description = viewmodel.Request.Description,
                    EstimatedTime = viewmodel.Request.EstimatedTime,
                    IsCompleted = viewmodel.Request.IsCompleted,
                    GroupId = viewmodel.Request.GroupId,
                    CategoryId = viewmodel.Request.CategoryId,
                    UserId = viewmodel.Request.UserId,
                    Bonus = viewmodel.Request.Bonus
                };
                _dealContext.Requests.Add(request);
                
                if(_dealContext.SaveChanges() == 0)
                {
                    throw new Exception("The RequestViewModel could not be saved!");
                }
                
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
            return View(viewmodel);
        }
        
        [HttpGet]
        public IActionResult Edit(Int32? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(400);
            
            var viewModel = new RequestViewModel
            {
                Request = _dealContext.Requests.FirstOrDefault(m => m.Id == id),
                Offers = _dealContext.Offers.Where(m => m.RequestId == null).AsEnumerable().OrderBy(m => m.Title),
                Groups = _dealContext.Groups.AsEnumerable().OrderBy(m => m.Name),
                Users = _dealContext.Users.Include(l => l.Profile).AsEnumerable().OrderByDescending(m => m.CreatedAt),
                Categories = _dealContext.Categories.AsEnumerable().OrderBy(m => m.Name)
            };
            
            return View(viewModel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(RequestViewModel model)
        {
            RequestViewModel viewModel = null;
            try 
            {
                if(!ModelState.IsValid)
                    throw new Exception("The Request model is not valid!");
                    
                var originalModel = _dealContext.Requests.FirstOrDefault(m => m.Id == model.Request.Id);
                
                if(originalModel == null)
                    throw new Exception("The existing Request: " + model.Request.Id + " doesn't exist anymore!");
                    
                if(model.Request.OfferId.HasValue)
                {
                    //ParentOffer? ==> inherit that group and cat
                    var offer = _dealContext.Offers.FirstOrDefault(m => m.Id == model.Request.OfferId);
                    originalModel.OfferId = model.Request.OfferId;
                    originalModel.GroupId = offer.GroupId;
                    originalModel.CategoryId = offer.CategoryId;
                }   
                else
                {
                    // No ParentOffer
                    originalModel.OfferId = null;
                    originalModel.GroupId = model.Request.GroupId;
                    originalModel.CategoryId = model.Request.CategoryId;
                }
                    
                originalModel.IsCompleted = model.Request.IsCompleted;
                originalModel.Title = model.Request.Title;
                originalModel.Description = model.Request.Description;
                originalModel.EstimatedTime = model.Request.EstimatedTime;
                originalModel.UserId = model.Request.UserId;
                originalModel.Bonus = model.Request.Bonus;
                
                _dealContext.Requests.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Request model could not be saved!");
                } 
                
                return RedirectToAction("Index");
            
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                
                viewModel = new RequestViewModel
                {
                    Request = new Request(),
                    Groups = _dealContext.Groups.AsEnumerable().OrderBy(m => m.Name),
                    Users = _dealContext.Users.Include(l => l.Profile).AsEnumerable().OrderByDescending(m => m.CreatedAt),
                    Categories = _dealContext.Categories.AsEnumerable().OrderBy(m => m.Name)
                };     
            }
            return View(viewModel);
        }
        
        [HttpPost]
        public IActionResult Delete(Int32? id)
        {
            try
            {
                var originalModel = _dealContext.Requests.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Request with id: " + id + " doesn't exist anymore!");

                _dealContext.Requests.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Deleted;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Request model could not be saved!");
                } 

                var msg = CreateMessage(ControllerActionType.Delete, "request", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Request");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "request", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Request");
                }
            }
        }
        
        [HttpPost]
        public IActionResult ToggleComplete(Int32? id)
        {
            try
            {
                var originalModel = _dealContext.Requests.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Request with id: " + id + " doesn't exist anymore!");
                    
                if (!originalModel.IsCompleted)
                    originalModel.IsCompleted = true;
                else
                    originalModel.IsCompleted = false;

                _dealContext.Requests.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Request model could not be saved!");
                } 

                var msg = CreateMessage(ControllerActionType.Edit, "request", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Request");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Edit, "request", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Request");
                }
            }
        }
        
        [HttpPost]
        public IActionResult SoftDelete(Int32 id)
        {
            try
            {
                var originalModel = _dealContext.Requests.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Request with id: " + id + " doesn't exist anymore!");

                originalModel.DeletedAt = DateTime.Now;
                _dealContext.Requests.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Request model could not be soft deleted!");
                } 

                var msg = CreateMessage(ControllerActionType.SoftDelete, "request", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Request");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.SoftDelete, "request", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Request");
                }
            }
        }
        
        [HttpPost]
        public IActionResult SoftUnDelete(Int32 id)
        {
            try
            {
                var originalModel = _dealContext.Requests.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Request with id: " + id + " doesn't exist anymore!");

                originalModel.DeletedAt = null;
                _dealContext.Requests.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Request model could not be soft undeleted!");
                } 

                var msg = CreateMessage(ControllerActionType.SoftUnDelete, "request", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Request");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.SoftUnDelete, "request", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Request");
                }
            }
        }
    }
}
