using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.AspNet.Authorization;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Storage;
using Deal.Models;
using Deal.Models.ViewModels;

namespace Deal.Web.Areas.Backoffice.Controllers
{
    [Area("Backoffice")]
    [Authorize(Roles = "Administrator")]
    public class OfferController : CommonController
    {
        [HttpGet]
        public IActionResult Index()
        {
            var model = _dealContext.Offers.AsEnumerable().OrderByDescending(m => m.CreatedAt);
            
            if(this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                return PartialView("_ListPartial", model);
            }
            
            return View(model);
        }
        
        [HttpGet]
        public IActionResult Create()
        {
            var viewmodel = new OfferViewModel
            {
              Offer = new Offer(),
              Requests = _dealContext.Requests.AsEnumerable().OrderBy(m => m.Title),
              Groups = _dealContext.Groups.AsEnumerable().OrderBy(m => m.Name),
              Users = _dealContext.Users.Include(l => l.Profile).AsEnumerable().OrderByDescending(m => m.CreatedAt),
              Categories = _dealContext.Categories.AsEnumerable().OrderBy(m => m.Name)
            };
            
            return View(viewmodel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(OfferViewModel viewmodel)
        {
            try
            {
                var offer = new Offer();
                
                if(!ModelState.IsValid)
                    throw new Exception("The OfferViewModel is not valid!");
                    
                if (viewmodel.Offer.RequestId.HasValue)
                {
                    // Parentrequest? => inherit that group and cat
                    offer.RequestId = viewmodel.Offer.RequestId;
                    var request = _dealContext.Requests.FirstOrDefault(m => m.Id == viewmodel.Offer.RequestId);
                    offer.GroupId = request.GroupId;
                    offer.CategoryId = request.CategoryId;
                }
                else
                {
                    // No request => take group and cat from viewmodel
                    offer.GroupId = viewmodel.Offer.GroupId;
                    offer.CategoryId = viewmodel.Offer.CategoryId;
                }
                offer.Title = viewmodel.Offer.Title;
                offer.Description = viewmodel.Offer.Description;
                offer.IsCompleted = viewmodel.Offer.IsCompleted;
                offer.UserId = viewmodel.Offer.UserId;
                
                _dealContext.Offers.Add(offer);
                
                if(_dealContext.SaveChanges() == 0)
                {
                    throw new Exception("The OfferViewModel could not be saved!");
                }
                
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
            return View(viewmodel);
        }
        
        [HttpGet]
        public IActionResult Edit(Int32? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(400);
            
            var viewModel = new OfferViewModel
            {
                Offer = _dealContext.Offers.FirstOrDefault(m => m.Id == id),
                Requests = _dealContext.Requests.Where(m => m.OfferId == null).AsEnumerable().OrderBy(m => m.Title),
                Groups = _dealContext.Groups.AsEnumerable().OrderBy(m => m.Name),
                Users = _dealContext.Users.Include(l => l.Profile).AsEnumerable().OrderByDescending(m => m.CreatedAt),
                Categories = _dealContext.Categories.AsEnumerable().OrderBy(m => m.Name)
            };
            
            return View(viewModel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(OfferViewModel model)
        {
            OfferViewModel viewModel = null;
            try 
            {
                if(!ModelState.IsValid)
                    throw new Exception("The Offer model is not valid!");
                    
                var originalModel = _dealContext.Offers.FirstOrDefault(m => m.Id == model.Offer.Id);
                
                if(originalModel == null)
                    throw new Exception("The existing Offer: " + model.Offer.Id + " doesn't exist anymore!");
                    
                if (model.Offer.RequestId.HasValue)
                {
                    // Parentrequest? => inherit that group and cat
                    originalModel.RequestId = model.Offer.RequestId;
                    var request = _dealContext.Requests.FirstOrDefault(m => m.Id == model.Offer.RequestId);
                    originalModel.GroupId = request.GroupId;
                    originalModel.CategoryId = request.CategoryId;
                }
                else
                {
                    // No request => take group and cat from viewmodel, set request to null
                    originalModel.RequestId = null;
                    originalModel.GroupId = model.Offer.GroupId;
                    originalModel.CategoryId = model.Offer.CategoryId;
                }
                originalModel.Title = model.Offer.Title;
                originalModel.Description = model.Offer.Description;
                originalModel.IsCompleted = model.Offer.IsCompleted;
                originalModel.UserId = model.Offer.UserId;
                
                _dealContext.Offers.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Offer model could not be saved!");
                } 
                
                return RedirectToAction("Index");
            
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                
                viewModel = new OfferViewModel
                {
                    Offer = new Offer(),
                    Requests = _dealContext.Requests.AsEnumerable().OrderBy(m => m.Title),
                    Groups = _dealContext.Groups.AsEnumerable().OrderBy(m => m.Name),
                    Users = _dealContext.Users.Include(l => l.Profile).AsEnumerable().OrderByDescending(m => m.CreatedAt),
                    Categories = _dealContext.Categories.AsEnumerable().OrderBy(m => m.Name)
                };
            }
            return View(viewModel);
        }
        
        [HttpPost]
        public IActionResult Delete(Int32 id)
        {
            try
            {
                var originalModel = _dealContext.Offers.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Offer with id: " + id + " doesn't exist anymore!");

                _dealContext.Offers.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Deleted;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Offer model could not be saved!");
                } 

                var msg = CreateMessage(ControllerActionType.Delete, "offer", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Offer");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "offer", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Offer");
                }
            }
        }
        
        [HttpPost]
        public IActionResult ToggleComplete(Int32 id)
        {
            try
            {
                var originalModel = _dealContext.Offers.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Offer with id: " + id + " doesn't exist anymore!");
                    
                if (!originalModel.IsCompleted)
                    originalModel.IsCompleted = true;
                else
                    originalModel.IsCompleted = false;

                _dealContext.Offers.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Offer model could not be saved!");
                } 

                var msg = CreateMessage(ControllerActionType.Edit, "offer", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Offer");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Edit, "offer", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Offer");
                }
            }
        }
        
        [HttpPost]
        public IActionResult SoftDelete(Int32 id)
        {
            try
            {
                var originalModel = _dealContext.Offers.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Offer with id: " + id + " doesn't exist anymore!");

                originalModel.DeletedAt = DateTime.Now;
                _dealContext.Offers.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Offer model could not be soft deleted!");
                } 

                var msg = CreateMessage(ControllerActionType.SoftDelete, "offer", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Offer");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.SoftDelete, "offer", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Offer");
                }
            }
        }
        
        [HttpPost]
        public IActionResult SoftUnDelete(Int32 id)
        {
            try
            {
                var originalModel = _dealContext.Offers.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Offer with id: " + id + " doesn't exist anymore!");

                originalModel.DeletedAt = null;
                _dealContext.Offers.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Offer model could not be soft undeleted!");
                } 

                var msg = CreateMessage(ControllerActionType.SoftUnDelete, "offer", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Offer");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.SoftUnDelete, "offer", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Offer");
                }
            }
        }
    }
}
