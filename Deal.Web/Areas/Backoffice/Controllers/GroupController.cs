using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Authorization;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Storage;
using Deal.Models;
using Deal.Models.ViewModels;

namespace Deal.Web.Areas.Backoffice.Controllers
{
    [Area("Backoffice")]
    [Authorize(Roles = "Administrator")]
    public class GroupController : CommonController
    {
        [HttpGet]
        public IActionResult Index()
        {
            var model = _dealContext.Groups.AsEnumerable().OrderBy(m => m.CreatedAt);
            
            if(this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                return PartialView("_ListPartial", model);
            }
            
            return View(model);
        }
        
        [HttpGet]
        public IActionResult Create()
        {   
            var model = new GroupViewModel
            {
                Group = new Group(),
                Requests = null,
                Offers = null,
                Users = _dealContext.Users.Include(l => l.Profile).AsEnumerable().OrderByDescending(m => m.CreatedAt)
            };
            
            return View(model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(GroupViewModel model)
        {
            try
            {
                if(!ModelState.IsValid)
                    throw new Exception("The Group model is not valid!");
                    
                var user = _dealContext.Users.FirstOrDefault(m => m.Id == model.Group.UserId);
                    
                var grp = new Group();
                
                var usersInGroup = new List<UserGroup>();
                usersInGroup.Add(new UserGroup
                {
                   UserId = model.Group.UserId,
                   GroupId = grp.Id 
                });
                
                grp.Name = model.Group.Name;
                grp.Description = model.Group.Description;
                grp.Users = usersInGroup;
                grp.UserId = model.Group.UserId;
                    
                _dealContext.Groups.Add(grp);
                
                if(_dealContext.SaveChanges() == 0)
                {
                    throw new Exception("The GroupViewModel could not be saved!");
                }
                
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
            return View(model);
        }
        
        [HttpGet]
        public IActionResult Edit(Int32? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(400);
            }
            
            var model = new GroupViewModel
            {
                Group = _dealContext.Groups.FirstOrDefault(m => m.Id == id),
                Requests = null,
                Offers = null,
                Users = _dealContext.Users.Include(l => l.Profile).AsEnumerable().OrderByDescending(m => m.CreatedAt)
            };
            
            if(model == null)
            {
                return RedirectToAction("Index");
            }
            
            return View(model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(GroupViewModel model)
        {
            try 
            {
                if(!ModelState.IsValid)
                    throw new Exception("The Group model is not valid!");
                    
                var originalModel = _dealContext.Groups.FirstOrDefault(m => m.Id == model.Group.Id);
                
                if(originalModel == null)
                    throw new Exception("The existing Group: " + model.Group.Name + " doesn't exist anymore!");
                    
                originalModel.Name = model.Group.Name;
                originalModel.Description = model.Group.Description;
                originalModel.UserId = model.Group.UserId;
                originalModel.UpdatedAt = null;
                
                _dealContext.Groups.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Group model could not be saved!");
                } 
                
                return RedirectToAction("Index");
            
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
            return View(model);
        }
        
        [HttpPost]
        public IActionResult Delete(Int32 id)
        {
            try
            {
                var originalModel = _dealContext.Groups.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Group with id: " + id + " doesn't exist anymore!");

                _dealContext.Groups.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Deleted;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Group model could not be saved!");
                } 

                var msg = CreateMessage(ControllerActionType.Delete, "group", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Group");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "group", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Group");
                }
            }
        }
        
        [HttpPost]
        public IActionResult SoftDelete(Int32 id)
        {
            try
            {
                var originalModel = _dealContext.Groups.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Group with id: " + id + " doesn't exist anymore!");

                originalModel.DeletedAt = DateTime.Now;
                _dealContext.Groups.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Group model could not be soft deleted!");
                } 

                var msg = CreateMessage(ControllerActionType.SoftDelete, "group", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Group");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.SoftDelete, "group", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Group");
                }
            }
        }
        
        [HttpPost]
        public IActionResult SoftUnDelete(Int32 id)
        {
            try
            {
                var originalModel = _dealContext.Groups.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Group with id: " + id + " doesn't exist anymore!");

                originalModel.DeletedAt = null;
                _dealContext.Groups.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Group model could not be soft undeleted!");
                } 

                var msg = CreateMessage(ControllerActionType.SoftUnDelete, "group", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Group");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.SoftUnDelete, "group", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Group");
                }
            }
        }
    }
}
