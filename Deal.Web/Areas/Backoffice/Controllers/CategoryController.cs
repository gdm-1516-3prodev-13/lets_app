using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.AspNet.Authorization;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Storage;
using Deal.Models;

namespace Deal.Web.Areas.Backoffice.Controllers
{
    [Area("Backoffice")]
    [Authorize(Roles = "Administrator")]
    public class CategoryController : CommonController
    {
        [HttpGet]
        public IActionResult Index()
        {
            var model = _dealContext.Categories.AsEnumerable().OrderByDescending(m => m.CreatedAt);
            
            if(this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                return PartialView("_ListPartial", model);
            }
            
            return View(model);
        }
        
        [HttpGet]
        public IActionResult Create()
        {
            var model = new Category();
            
            return View(model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Category model)
        {
            try
            {
                if(!ModelState.IsValid)
                    throw new Exception("The Category model is not valid!");
                    
                _dealContext.Categories.Add(model);
                if(_dealContext.SaveChanges() == 0)
                {
                    throw new Exception("The Category model could not be saved!");
                }
                
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
            return View(model);
        }
        
        [HttpGet]
        public IActionResult Edit(Int16? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(400);
            }
            
            var model = _dealContext.Categories.FirstOrDefault(m => m.Id == id);
            
            if(model == null)
            {
                return RedirectToAction("Index");
            }
            
            return View(model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Category model)
        {
            try 
            {
                if(!ModelState.IsValid)
                    throw new Exception("The Category model is not valid!");
                    
                var originalModel = _dealContext.Categories.FirstOrDefault(m => m.Id == model.Id);
                
                if(originalModel == null)
                    throw new Exception("The existing Category: " + model.Name + " doesn't exist anymore!");
                    
                originalModel.Name = model.Name;
                originalModel.Description = model.Description;
                originalModel.UpdatedAt = null;
                
                _dealContext.Categories.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Category model could not be saved!");
                } 
                
                return RedirectToAction("Index");
            
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
            return View(model);
        }
        
        [HttpPost]
        public IActionResult Delete(Int16 id)
        {
            try
            {
                var originalModel = _dealContext.Categories.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Category with id: " + id + " doesn't exist anymore!");

                _dealContext.Categories.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Deleted;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Category model could not be saved!");
                } 

                var msg = CreateMessage(ControllerActionType.Delete, "category", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Category");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "category", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Category");
                }
            }
        }
        
        [HttpPost]
        public IActionResult SoftDelete(Int32 id)
        {
            try
            {
                var originalModel = _dealContext.Categories.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Category with id: " + id + " doesn't exist anymore!");

                originalModel.DeletedAt = DateTime.Now;
                _dealContext.Categories.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Category model could not be soft deleted!");
                } 

                var msg = CreateMessage(ControllerActionType.SoftDelete, "category", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Category");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.SoftDelete, "category", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Category");
                }
            }
        }
        
        [HttpPost]
        public IActionResult SoftUnDelete(Int32 id)
        {
            try
            {
                var originalModel = _dealContext.Categories.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Category with id: " + id + " doesn't exist anymore!");

                originalModel.DeletedAt = null;
                _dealContext.Categories.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Category model could not be soft undeleted!");
                } 

                var msg = CreateMessage(ControllerActionType.SoftUnDelete, "category", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Category");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.SoftUnDelete, "category", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Category");
                }
            }
        }
    }
}
