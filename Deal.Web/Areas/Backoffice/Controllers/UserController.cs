using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Storage;
using Deal.Models;
using Deal.Models.Identity;
using Deal.Models.Identity.ViewModels;

namespace Deal.Web.Areas.Backoffice.Controllers
{
    [Area("Backoffice")]
    [Authorize(Roles = "Administrator")]
    public class UserController : CommonController
    {   
        [HttpGet]
        public IActionResult Index()
        {
            var model = _dealContext.Users.Include(l => l.Profile).AsEnumerable().OrderByDescending(m => m.CreatedAt);            
            
            if(this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                return PartialView("_ListPartial", model);
            }
            
            return View(model);
        }
        
        [HttpGet]
        public IActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(400);
            }
            
            var profile = _dealContext.Profiles.FirstOrDefault(m => m.UserId == id);
            var user = _dealContext.Users.FirstOrDefault(m => m.Id == id);
            
            if(profile == null || user == null)
                return RedirectToAction("Index");
                
            /*string[] ids = null;
            if(user.Roles != null && user.Roles.Count > 0){
                ids = new string[user.Roles.Count];
                int i = 0;
                foreach (var role in user.Roles)
                {
                    ids[i] = role.RoleId;
                    i++;
                }
            }
            
            var filteredRoles = _dealContext.Roles.AsEnumerable();*/
            var viewModel = new AdminProfileViewModel
            {
                User = user,
                Profile = profile
            };
            
            return View(viewModel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(AdminProfileViewModel model)
        {
            AdminProfileViewModel viewModel = null;
            try 
            {
                if(!ModelState.IsValid)
                    throw new Exception("The User model is not valid!");
                    
                var originalProfile = _dealContext.Profiles.FirstOrDefault(m => m.UserId == model.Profile.UserId);
                var originalUser = _dealContext.Users.FirstOrDefault(m => m.Id == model.Profile.UserId);
                
                if(originalProfile == null)
                    throw new Exception("The existing Profile: " + model.Profile.FirstName + " " + model.Profile.LastName + " doesn't exist anymore!");
                    
                originalProfile.FirstName = model.Profile.FirstName;
                originalProfile.LastName = model.Profile.LastName;
                originalProfile.Description = model.Profile.Description;
                originalProfile.Location = model.Profile.Location;
                originalProfile.Points = model.Profile.Points;
                originalUser.UserName = model.User.UserName;
                originalUser.Email = model.User.Email;
                originalUser.NormalizedEmail = model.User.Email.ToUpper();
                
                _dealContext.Profiles.Attach(originalProfile);
                _dealContext.Entry(originalProfile).State = EntityState.Modified;
                
                _dealContext.Users.Attach(originalUser);
                _dealContext.Entry(originalUser).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The User model could not be saved!");
                }
                
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                
                var originalProfile = _dealContext.Profiles.FirstOrDefault(m => m.UserId == model.Profile.UserId);
                var originalUser = _dealContext.Users.FirstOrDefault(m => m.Id == model.Profile.UserId);
                
                viewModel = new AdminProfileViewModel
                {
                    User = originalUser,
                    Profile = originalProfile
                };
            }
            return View(viewModel);
        }
        
        [HttpPost]
        public IActionResult Delete(string id)
        {
            try
            {
                var userId = User.GetUserId();
                if(userId == id)
                    throw new Exception("You can't delete your own account");
                
                var originalModel = _dealContext.Users.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing User with id: " + id + " doesn't exist anymore!");

                _dealContext.Users.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Deleted;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The User model could not be saved!");
                } 

                var msg = CreateMessage(ControllerActionType.Delete, "user", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "user", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }
            }
        }
        
        [HttpPost]
        public IActionResult SoftDelete(string id)
        {
            try
            {
                var userId = User.GetUserId();
                if(userId == id)
                    throw new Exception("You can't softdelete your own account");
                
                var originalModel = _dealContext.Users.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing User with id: " + id + " doesn't exist anymore!");

                originalModel.DeletedAt = DateTime.Now;
                _dealContext.Users.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The User model could not be soft deleted!");
                } 

                var msg = CreateMessage(ControllerActionType.SoftDelete, "user", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.SoftDelete, "user", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }
            }
        }
        
        [HttpPost]
        public IActionResult SoftUnDelete(string id)
        {
            try
            {
                var originalModel = _dealContext.Users.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing User with id: " + id + " doesn't exist anymore!");

                originalModel.DeletedAt = null;
                _dealContext.Users.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The User model could not be soft undeleted!");
                } 

                var msg = CreateMessage(ControllerActionType.SoftUnDelete, "user", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.SoftUnDelete, "user", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }
            }
        }
        
        [HttpPost]
        public async Task<ActionResult> Lock(string id)
        {
            try
            {
                var userId = User.GetUserId();
                if(userId == id)
                    throw new Exception("You can't lock your own account");
                
                var model = await _applicationUserManager.FindByIdAsync(id);

                if (model == null)
                    throw new Exception("User does not exist!");

                var now = DateTime.Now;
                now = now.AddMonths(1);

                if (await _applicationUserManager.GetLockoutEnabledAsync(model) == false)
                    throw new Exception("Lockout is not enabled!");

                var result = await _applicationUserManager.SetLockoutEndDateAsync(model, new DateTimeOffset(now));

                if (result.Succeeded)
                {
                    var msg = CreateMessage(ControllerActionType.Lock, "user", model.UserName);

                    if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                    {
                        return Json(new { state = 1, id = model.Id, message = msg });
                    }
                    else
                    {
                        return RedirectToAction("Index", "User");
                    }
                }
                else
                {
                    throw new Exception("Can't lock the user!");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Lock, "user", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }
            }
        }

        [HttpPost]
        public async Task<ActionResult> UnLock(string id)
        {
            try
            {
                var model = await _applicationUserManager.FindByIdAsync(id);

                if (model == null)
                    throw new Exception("User does not exist!");

                if (await _applicationUserManager.GetLockoutEnabledAsync(model) == false)
                    throw new Exception("Lockout is not enabled!");

                var result = await _applicationUserManager.SetLockoutEndDateAsync(model, DateTimeOffset.MinValue);

                if (result.Succeeded)
                {
                    var msg = CreateMessage(ControllerActionType.Lock, "user", model.UserName);

                    if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                    {
                        return Json(new { state = 1, id = model.Id, message = msg });
                    }
                    else
                    {
                        return RedirectToAction("Index", "User");
                    }
                }
                else
                {
                    throw new Exception("Can't unlock the user!");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Lock, "user", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }
            }
        }
    }
}
