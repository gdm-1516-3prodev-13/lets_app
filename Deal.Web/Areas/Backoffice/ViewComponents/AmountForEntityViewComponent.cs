using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Deal.Models;
using Deal.Models.ViewModels;
using Deal.Data;

namespace Deal.Web.Areas.Backoffice.ViewComponents
{
    public class AmountForEntityViewComponent : ViewComponent
    {
        private readonly DealDbContext _dealContext;
        
        public AmountForEntityViewComponent(DealDbContext dealContext)
        {
            _dealContext = dealContext;
        }
        
        public IViewComponentResult Invoke(string entityType)
        {
            var model = GetAmountForEntity(entityType);
            return View(model);
        }

        public async Task<IViewComponentResult> InvokeAsync(string entityType)
        {
            var model = await GetAmountForEntityAsync(entityType);
            return View(model);
        }

        private Task<AmountForEntityViewModel> GetAmountForEntityAsync(string entityType)
        {
            return Task.FromResult(GetAmountForEntity(entityType));

        }
        private AmountForEntityViewModel GetAmountForEntity(string entityType)
        {
            var amount = 0;
            var entitytype = "Entity";
            var name = "Entity";
            var pluralizeName = "Entities";
            
            switch(entityType)
            {
                case "Group":
                    amount = _dealContext.Groups.AsEnumerable().Count();
                    entitytype = "Group";
                    name = "Group";
                    pluralizeName = "Groups";
                    break;
                case "User":
                    amount = _dealContext.Users.AsEnumerable().Count();
                    entitytype = "User";
                    name = "User";
                    pluralizeName = "Users";
                    break;
                case "Role":
                    amount = _dealContext.Roles.AsEnumerable().Count();
                    entitytype = "Role";
                    name = "Role";
                    pluralizeName = "Roles";
                    break;
                case "Category":
                    amount = _dealContext.Categories.AsEnumerable().Count();
                    entitytype = "Category";
                    name = "Category";
                    pluralizeName = "Categories";
                    break;
                case "Request":
                    amount = _dealContext.Requests.AsEnumerable().Count();
                    entitytype = "Request";
                    name = "Request";
                    pluralizeName = "Requests";
                    break;
                case "Offer":
                    amount = _dealContext.Offers.AsEnumerable().Count();
                    entitytype = "Offer";
                    name = "Offer";
                    pluralizeName = "Offers";
                    break;
            }
            
            var model = new AmountForEntityViewModel()
            {
                Amount = amount,
                EntityType = entitytype,
                Name = name,
                PluralizeName = pluralizeName
            };

            return model;
        }

    }
}