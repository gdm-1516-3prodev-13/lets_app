using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Authorization;
using Microsoft.Data.Entity;
using Deal.Models;
using Deal.Models.ViewModels;

namespace Deal.Web.Controllers
{
    [Authorize]
    public class RequestController : CommonController
    {
        [HttpGet]
        public IActionResult Index()
        {
            var model = new RequestsListViewModel();
            
            var userId = "";
            if (User.IsSignedIn())
                userId = User.GetUserId();
            else
                return RedirectToAction("Index", "Home");
                
            var openReqs = _dealContext.Requests
                .Where(m => m.UserId == userId)
                .Where(m => m.DeletedAt == null)
                .Where(m => m.IsCompleted == false)
                .Include(m => m.Group)
                .Include(m => m.Offers)
                .AsEnumerable();
            var completedReqs = _dealContext.Requests
                .Where(m => m.UserId == userId)
                .Where(m => m.DeletedAt == null)
                .Where(m => m.IsCompleted == true)
                .Include(m => m.Group)
                .Include(m => m.Offers)
                .AsEnumerable();
                
            model.OpenRequests = openReqs;
            model.CompletedRequests = completedReqs;
            
            return View(model);
        }
        
        [HttpGet]
        public IActionResult Create(Int32 grpId, Int32? ofrId)
        {
            var grp = _dealContext.Groups.Where(m => m.DeletedAt == null).FirstOrDefault(m => m.Id == grpId);
            if(grp == null)
                throw new Exception("Couldn't find your group");
                
            var userId = User.GetUserId();
            var request = new Request
            {
                UserId = userId,
                GroupId = grpId,
                Group = grp,
                EstimatedTime = 30
            };
                
            var model = new RequestViewModel
            {
                Request = request,
                Categories = _dealContext.Categories.Where(m => m.DeletedAt == null).AsEnumerable()
            };
            
            if(ofrId != null)
            {
                var ofr = _dealContext.Offers.FirstOrDefault(m => m.Id == ofrId);
                request.OfferId = ofrId;
                request.ParentOffer = ofr;
                request.Category = ofr.Category;
                request.CategoryId = ofr.CategoryId;
            }
            
            return View(model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(RequestViewModel viewmodel)
        {
            try
            {
                if(!ModelState.IsValid)
                    throw new Exception("The RequestViewModel is not valid!");  
                        
                var request = new Request
                {
                    Title = viewmodel.Request.Title,
                    Description = viewmodel.Request.Description,
                    EstimatedTime = viewmodel.Request.EstimatedTime,
                    IsCompleted = false,
                    GroupId = viewmodel.Request.GroupId,
                    UserId = viewmodel.Request.UserId,
                    CategoryId = viewmodel.Request.CategoryId,
                    Bonus = viewmodel.Request.Bonus
                };
                
                if(viewmodel.Request.OfferId != null)
                    request.OfferId = viewmodel.Request.OfferId;
                    
                _dealContext.Requests.Add(request);
                
                if(_dealContext.SaveChanges() == 0)
                {
                    throw new Exception("The RequestViewModel could not be saved!");
                }
                
                return RedirectToAction("Details", "Request", new { @id = request.Id });
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
            return View(viewmodel);
        }
        
        [HttpGet]
        public IActionResult Details(Int32? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(400);
            
            var request = _dealContext.Requests
                .Include(m => m.User)
                .Include(m => m.Group)
                .Include(m => m.Category)
                .Include(m => m.ParentOffer)
                .Include(m => m.Offers)
                .FirstOrDefault(m => m.Id == id);
            
            if(request == null)
                return new HttpStatusCodeResult(400);
            
            var viewModel = new RequestViewModel
            {
                Request = request,
                AcceptedOffer = _dealContext.Offers.Where(m => m.RequestId == id).Where(m => m.IsCompleted == true).FirstOrDefault(),
                Offers = _dealContext.Offers.Include(m => m.User).Where(m => m.DeletedAt == null).Where(m => m.RequestId == id),
                Comments = _dealContext.Comments.Include(m => m.User).Where(m => m.DeletedAt == null).Where(m => m.RequestId == id),
                NewComment = new Comment()
            };
                
            var userId = "";
            if (User.IsSignedIn())
            {
                userId = User.GetUserId();
                ViewBag.UserId = userId;
            }

            return View(viewModel);
        }
        
        [HttpGet]
        public IActionResult Edit(Int32? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(400);
            }
            
            var viewModel = new RequestViewModel
            {
                Request = _dealContext.Requests.FirstOrDefault(m => m.Id == id),
                Categories = _dealContext.Categories.AsEnumerable().OrderBy(m => m.Name),
            };
            
            if(viewModel == null)
                return RedirectToAction("Index");
            
            var userId = "";
            if (User.IsSignedIn())
                userId = User.GetUserId();
            
            if(viewModel.Request.UserId != userId)
                return RedirectToAction("Index");
                
            return View(viewModel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(RequestViewModel model)
        {
            RequestViewModel viewModel = null;
            try 
            {
                if(!ModelState.IsValid)
                    throw new Exception("The Request model is not valid!");
                    
                var originalModel = _dealContext.Requests.FirstOrDefault(m => m.Id == model.Request.Id);
                
                if(originalModel == null)
                    throw new Exception("The existing Request: " + model.Request.Id + " doesn't exist anymore!");
                    
                originalModel.Title = model.Request.Title;
                originalModel.Description = model.Request.Description;
                originalModel.EstimatedTime = model.Request.EstimatedTime;
                originalModel.CategoryId = model.Request.CategoryId;
                originalModel.Bonus = model.Request.Bonus;
                
                _dealContext.Requests.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Request model could not be saved!");
                } 
                
                return RedirectToAction("Details", new { id = model.Request.Id });
            
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.InnerException.ToString());
                
                viewModel = new RequestViewModel
                {
                    Request = new Request(),
                    Groups = _dealContext.Groups.AsEnumerable().OrderBy(m => m.Name),
                    Users = _dealContext.Users.Include(l => l.Profile).AsEnumerable().OrderByDescending(m => m.CreatedAt),
                    Categories = _dealContext.Categories.AsEnumerable().OrderBy(m => m.Name)
                };     
            }
            return View(viewModel);
        }
        
        [HttpPost]
        public IActionResult Delete(Int32? id)
        {   
            try
            {
                var originalModel = _dealContext.Requests.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Request with id: " + id + " doesn't exist anymore!");
                
                originalModel.DeletedAt = DateTime.Now;
                _dealContext.Requests.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Request model could not be deleted!");
                } 
                
                var msg = CreateMessage(ControllerActionType.Delete, "request", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Request");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "request", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Request");
                }
            }
        }
        
        [HttpPost]
        public IActionResult AcceptOffer(Int32 reqId, Int32 offId)
        {
            try
            {
                var request = _dealContext.Requests.Where(m => m.DeletedAt == null).FirstOrDefault(m => m.Id == reqId);
                var offer = _dealContext.Offers.Where(m => m.DeletedAt == null).FirstOrDefault(m => m.Id == offId);
                
                if(request == null || offer == null)
                    throw new Exception("The offermodel and/or requestmodel don't exist anymore!");
                    
                var userId = "";
                if (User.IsSignedIn())
                    userId = User.GetUserId();
                else
                    return RedirectToAction("Index", "Home");

                var usrRequest = _dealContext.Profiles.FirstOrDefault(m => m.UserId == userId);
                var usrOffer = _dealContext.Profiles.FirstOrDefault(m => m.UserId == offer.UserId);
                
                // Calculate points
                double pointsPerMinute = 1;
                double basepoints = (request.EstimatedTime * pointsPerMinute);
                double bonus = (double)request.Bonus.Value/100;
                double totalBonus = bonus + 1;
                Int32 totalPoints = Convert.ToInt32(basepoints * totalBonus);
                
                // Detract points from usrRequest
                usrRequest.Points -= totalPoints;
                _dealContext.Profiles.Attach(usrRequest);
                _dealContext.Entry(usrRequest).State = EntityState.Modified;
                
                // Give points to usrOffer 
                usrOffer.Points += totalPoints;
                _dealContext.Profiles.Attach(usrOffer);
                _dealContext.Entry(usrOffer).State = EntityState.Modified;
                
                // Complete Offer + Request
                offer.IsCompleted = true;
                _dealContext.Offers.Attach(offer);
                _dealContext.Entry(offer).State = EntityState.Modified;
                
                request.IsCompleted = true;
                _dealContext.Requests.Attach(request);
                _dealContext.Entry(request).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Request model could not be saved!");
                } 

                var msg = "";
                msg = CreateMessage(ControllerActionType.Complete, "request", reqId);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = reqId, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Request");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Edit, "request", reqId, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = reqId, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Request");
                }
            }
        }
    }
}