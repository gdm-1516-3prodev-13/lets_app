using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Authorization;
using Microsoft.Data.Entity;
using Deal.Models;
using Deal.Models.ViewModels;

namespace Deal.Web.Controllers
{
    [Authorize]
    public class GroupController : CommonController
    {
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Index()
        {
            var model = new GroupsListViewModel();
            
            var userId = User.GetUserId();
            
            var allGrps = _dealContext.Groups
                .Where(m => m.DeletedAt == null)
                .Include(m => m.Requests)
                .Include(m => m.Offers)
                .Include(m => m.Users)
                .AsEnumerable();
            var subGrps = _dealContext.Groups
                .Where(m => m.Users.Any(u => u.UserId == userId))
                .Where(m => m.DeletedAt == null)
                .AsEnumerable();
            var openGrps = allGrps.Except(subGrps).AsEnumerable();
            
            model.SubscribedGroups = subGrps;
            model.OpenGroups = openGrps;

            return View(model);
        }
        
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Details(Int32 id)
        {
            var userId = "";
            userId = User.GetUserId();
            
            var grp = _dealContext.Groups.FirstOrDefault(m => m.Id == id);
            
            var model = new GroupViewModel
            {
                Group = grp,
                Requests = _dealContext.Requests.Where(m => m.GroupId == id).Where(m => m.DeletedAt == null).Where(m => m.ParentOffer == null).Include(m => m.Offers).AsEnumerable(),
                Offers = _dealContext.Offers.Where(m => m.GroupId == id).Where(m => m.DeletedAt == null).Where(m => m.ParentRequest == null).Include(m => m.Requests).AsEnumerable(),
                Users = _dealContext.Users.Where(m => m.SubscribedGroups.Any(g => g.GroupId == grp.Id)).AsEnumerable(),
                IsSubscribed = false
            };
            
            var isUserSubscribed = _dealContext.UserGroups.Where(m => m.GroupId == id).FirstOrDefault(m => m.UserId == userId);
            if(isUserSubscribed != null)
                model.IsSubscribed = true;

            return View(model);
        }
        
        [HttpGet]
        public IActionResult Create()
        {
            var userId = User.GetUserId();
            
            var model = new Group{
                UserId = userId
            };
            return View(model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Group model)
        {
            try
            {
                if(!ModelState.IsValid)
                    throw new Exception("The Group model is not valid!");
                    
                var userId = User.GetUserId();
                if(userId == null || userId == "")
                    throw new Exception("Can't find the User ID");
                
                var grp = new Group();
                
                var usersInGroup = new List<UserGroup>();
                usersInGroup.Add(new UserGroup
                {
                   UserId = userId,
                   GroupId = grp.Id 
                });
                
                grp.Name = model.Name;
                grp.Description = model.Description;
                grp.Users = usersInGroup;
                grp.UserId = userId;
                
                _dealContext.Groups.Add(grp);
                
                if(_dealContext.SaveChanges() == 0)
                {
                    throw new Exception("The Group model could not be saved!");
                }
                
                return RedirectToAction("Index");
            }       
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
            return View(model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Join(Int32 grpId)
        {
            try
            {
                if(grpId == 0)
                    throw new Exception("No group specified");
                       
                var userId = User.GetUserId();
                if(userId == null || userId == "")
                    throw new Exception("Can't find the User ID");
                
                var usergrp = new UserGroup
                {
                    UserId = userId,
                    GroupId = grpId
                };
                
                _dealContext.UserGroups.Add(usergrp);
                
                if(_dealContext.SaveChanges() == 0)
                {
                    throw new Exception("The Group model could not be saved!");
                }
                
                return RedirectToAction("Details", "Group", new { @id = grpId });
            }       
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Unable to save changes");
                throw ex;
            }
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Leave(Int32 grpId)
        {
            try
            {       
                if(grpId == 0)
                    throw new Exception("No group specified");
                
                var userId = User.GetUserId();
                if(userId == null || userId == "")
                    throw new Exception("Can't find the User ID");
                    
                var grp = _dealContext.Groups.FirstOrDefault(m => m.Id == grpId);
                var grpToLeave = _dealContext.UserGroups.Where(m => m.GroupId == grpId).FirstOrDefault(m => m.UserId == userId);
                
                if(grp.UserId != userId)
                {
                     _dealContext.UserGroups.Remove(grpToLeave);    
                }
                else
                {
                    throw new Exception("you can't leave your own group");
                }
                

                if(_dealContext.SaveChanges() == 0)
                {
                    throw new Exception("The Group model could not be saved!");
                }
                
                return RedirectToAction("Index", "Group");
            }       
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Unable to save changes");
                throw ex;
            }
        }
        
        [HttpGet]
        public IActionResult CreateRequest()
        {
            var model = new RequestViewModel
            {
                Request = new Request(),
                Groups = null,
                Users = null,
                Categories = _dealContext.Categories.OrderBy(m => m.Name).AsEnumerable()
            };
            return View(model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateRequest(RequestViewModel model)
        {
            try
            {
                if(!ModelState.IsValid)
                    throw new Exception("The RequestViewModel is not valid");
                    
                var request = new Request
                {
                    Title = model.Request.Title,
                    Description = model.Request.Description,
                    EstimatedTime = model.Request.EstimatedTime,
                    IsCompleted = false
                };
                
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                 ModelState.AddModelError(string.Empty, "Unable to save changes.");
                 throw ex;
            }   
        }
        
    }
}