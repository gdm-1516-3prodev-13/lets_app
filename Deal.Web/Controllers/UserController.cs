using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Authorization;
using Microsoft.Data.Entity;
using Deal.Models;
using Deal.Models.Identity.ViewModels;

namespace Deal.Web.Controllers
{
    [AllowAnonymous]
    public class UserController : CommonController
    {   
        [Authorize]
        [HttpGet]
        public IActionResult Index()
        {
            var userId = User.GetUserId();
            if(userId == null)
                return RedirectToAction("Index", "Home");
            
            var model = new ProfileViewModel
            {
                User = _dealContext.Users.FirstOrDefault(m => m.Id == userId),
                Profile = _dealContext.Profiles.FirstOrDefault(m => m.UserId == userId),
                Groups = _dealContext.Groups.Where(m => m.Users.Any(u => u.UserId == userId)).Where(m => m.DeletedAt == null).AsEnumerable(),
                Requests = _dealContext.Requests.Where(m => m.UserId == userId).Where(m => m.DeletedAt == null).AsEnumerable(),
                Offers = _dealContext.Offers.Where(m => m.UserId == userId).Where(m => m.DeletedAt == null).AsEnumerable()
            };
                
            return View(model);
        }
        
        [HttpGet]
        public IActionResult Details(string id)
        {
            var userId = User.GetUserId();
            if(userId == id)
                return RedirectToAction("Index");
            
            var model = new ProfileViewModel
            {
                User = _dealContext.Users.FirstOrDefault(m => m.Id == id),
                Profile = _dealContext.Profiles.FirstOrDefault(m => m.UserId == id),
                Groups = _dealContext.Groups.Where(m => m.Users.Any(u => u.UserId == id)).Where(m => m.DeletedAt == null).AsEnumerable(),
                Requests = _dealContext.Requests.Where(m => m.UserId == id).Where(m => m.DeletedAt == null).AsEnumerable(),
                Offers = _dealContext.Offers.Where(m => m.UserId == id).Where(m => m.DeletedAt == null).AsEnumerable()
            };
                
            return View(model);
        }
        
        [HttpGet]
        public IActionResult Edit()
        {
            var userId = User.GetUserId();
            if(userId == null)
                return RedirectToAction("Index", "Home");
            
            var model = new ProfileViewModel
            {
                User = _dealContext.Users.FirstOrDefault(m => m.Id == userId),
                Profile = _dealContext.Profiles.FirstOrDefault(m => m.UserId == userId),
            };
                
            return View(model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(ProfileViewModel model)
        {
            try 
            {
                if(!ModelState.IsValid)
                    throw new Exception("The ProfileViewModel is not valid!");
                    
                var userId = User.GetUserId();
                if(userId == null)
                    return RedirectToAction("Index", "Home");
                    
                var originalUser = _dealContext.Users.FirstOrDefault(m => m.Id == userId);
                var originalProfile = _dealContext.Profiles.FirstOrDefault(m => m.UserId == userId);
                
                if(originalUser == null || originalProfile == null)
                    throw new Exception("The existing User: " + userId + " doesn't exist anymore!");
                    
                //originalUser.UserName = model.User.UserName;
                originalUser.Email = model.User.Email;
                originalUser.NormalizedEmail = model.User.Email.ToUpper();
                originalProfile.FirstName = model.Profile.FirstName;
                originalProfile.LastName = model.Profile.LastName;
                originalProfile.Location = model.Profile.Location;
                originalProfile.Description = model.Profile.Description;
                
                _dealContext.Users.Attach(originalUser);
                _dealContext.Entry(originalUser).State = EntityState.Modified;
                
                _dealContext.Profiles.Attach(originalProfile);
                _dealContext.Entry(originalProfile).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The models could not be saved!");
                } 
                
                return RedirectToAction("Index");
            
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);  
            }
            
            return RedirectToAction("Index");
        }
    }
}