using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Authorization;
using Microsoft.Data.Entity;
using Deal.Models;
using Deal.Models.ViewModels;

namespace Deal.Web.Controllers
{
    [Authorize]
    public class OfferController : CommonController
    {
        [HttpGet]
        public IActionResult Index()
        {
            var model = new OffersListViewModel();
            
            var userId = "";
            if (User.IsSignedIn())
                userId = User.GetUserId();
            else
                return RedirectToAction("Index", "Home");
                
            var openOffs = _dealContext.Offers
                .Where(m => m.UserId == userId)
                .Where(m => m.DeletedAt == null)
                .Where(m => m.IsCompleted == false)
                .Include(m => m.Group)
                .AsEnumerable();
            var completedOffs = _dealContext.Offers
                .Where(m => m.UserId == userId)
                .Where(m => m.DeletedAt == null)
                .Where(m => m.IsCompleted == true)
                .Include(m => m.Group)
                .AsEnumerable();
                
            model.OpenOffers = openOffs;
            model.CompletedOffers = completedOffs;
            
            return View(model);
        }
        
        [HttpGet]
        public IActionResult Create(Int32 grpId, Int32? reqId)
        {
            var grp = _dealContext.Groups.Where(m => m.DeletedAt == null).FirstOrDefault(m => m.Id == grpId);
            if(grp == null)
                throw new Exception("Couldn't find your group");
                
            var userId = User.GetUserId();
            var offer = new Offer
            {
                UserId = userId,
                GroupId = grpId,
                Group = grp
            };
            
            var model = new OfferViewModel();
            model.Offer = offer;
            model.Categories = _dealContext.Categories.Where(m => m.DeletedAt == null).AsEnumerable();
            
            if (reqId != null)
            {
                var req = _dealContext.Requests.FirstOrDefault(m => m.Id == reqId);
                offer.RequestId = reqId;
                offer.ParentRequest = req;
                offer.Category = req.Category;
                offer.CategoryId = req.CategoryId;
            }
            
            return View(model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(OfferViewModel viewmodel)
        {
            try
            {
                if(!ModelState.IsValid)
                    throw new Exception("The RequestViewModel is not valid!");
                    
                var offer = new Offer
                {
                    Title = viewmodel.Offer.Title,
                    Description = viewmodel.Offer.Description,
                    IsCompleted = false,
                    GroupId = viewmodel.Offer.GroupId,
                    UserId = viewmodel.Offer.UserId,
                    CategoryId = viewmodel.Offer.CategoryId
                };
                
                if(viewmodel.Offer.RequestId != null)
                    offer.RequestId = viewmodel.Offer.RequestId;
                
                _dealContext.Offers.Add(offer);
                
                if(_dealContext.SaveChanges() == 0)
                {
                    throw new Exception("The RequestViewModel could not be saved!");
                }
                
                return RedirectToAction("Details", "Offer", new { @id = offer.Id });
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
            return View(viewmodel);
        }
        
        [HttpGet]
        public IActionResult Details(Int32? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(400);
            
            var offer = _dealContext.Offers
                .Include(m => m.User)
                .Include(m => m.Group)
                .Include(m => m.Category)
                .Include(m => m.ParentRequest)
                .Include(m => m.Requests)
                .FirstOrDefault(m => m.Id == id);
            
            if(offer == null)
                return new HttpStatusCodeResult(400);
            
            var viewModel = new OfferViewModel
            {
                Offer = offer,
                AcceptedRequest = _dealContext.Requests.Where(m => m.OfferId == id).Where(m => m.IsCompleted == true).FirstOrDefault(),
                Requests = _dealContext.Requests.Include(m => m.User).Where(m => m.DeletedAt == null).Where(m => m.OfferId == id),
                Comments = _dealContext.Comments.Include(m => m.User).Where(m => m.DeletedAt == null).Where(m => m.OfferId == id),
                NewComment = new Comment()
            };
                
            var userId = "";
            if (User.IsSignedIn())
            {
                userId = User.GetUserId();
                ViewBag.UserId = userId;
            }

            return View(viewModel);
        }
        
        [HttpGet]
        public IActionResult Edit(Int32? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(400);
            
            var viewModel = new OfferViewModel
            {
                Offer = _dealContext.Offers.FirstOrDefault(m => m.Id == id),
                Requests = _dealContext.Requests.Where(m => m.DeletedAt == null).OrderBy(m => m.Title).AsEnumerable(),
                Categories = _dealContext.Categories.Where(m => m.DeletedAt == null).AsEnumerable().OrderBy(m => m.Name),
            };
            
            if(viewModel == null)
                return RedirectToAction("Index");
            
            var userId = "";
            if (User.IsSignedIn())
                userId = User.GetUserId();
            
            if(viewModel.Offer.UserId != userId)
                return RedirectToAction("Index");
                
            return View(viewModel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(OfferViewModel model)
        {
            OfferViewModel viewModel = null;
            try 
            {
                if(!ModelState.IsValid)
                    throw new Exception("The Offer model is not valid!");
                    
                var originalModel = _dealContext.Offers.FirstOrDefault(m => m.Id == model.Offer.Id);
                
                if(originalModel == null)
                    throw new Exception("The existing Offer: " + model.Offer.Id + " doesn't exist anymore!");
                    
                originalModel.Title = model.Offer.Title;
                originalModel.Description = model.Offer.Description;
                originalModel.CategoryId = model.Offer.CategoryId;
                
                _dealContext.Offers.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Offer model could not be saved!");
                } 
                
                return RedirectToAction("Details", new { id = model.Offer.Id });
            
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                
                viewModel = new OfferViewModel
                {
                    Offer = new Offer(),
                    Requests = _dealContext.Requests.Where(m => m.DeletedAt == null).AsEnumerable().OrderBy(m => m.Title),
                    Categories = _dealContext.Categories.Where(m => m.DeletedAt == null).AsEnumerable().OrderBy(m => m.Name)
                };     
            }
            return View(viewModel);
        }
        
        [HttpPost]
        public IActionResult Delete(Int32? id)
        {   
            try
            {
                var originalModel = _dealContext.Requests.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Request with id: " + id + " doesn't exist anymore!");
                
                originalModel.DeletedAt = DateTime.Now;
                _dealContext.Requests.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Request model could not be deleted!");
                } 
                
                var msg = CreateMessage(ControllerActionType.Delete, "request", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Request");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "request", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Request");
                }
            }
        }
        
        [HttpPost]
        public IActionResult ToggleComplete(Int32? id)
        {
            try
            {
                var originalModel = _dealContext.Requests.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Request with id: " + id + " doesn't exist anymore!");
                    
                if (!originalModel.IsCompleted)
                    originalModel.IsCompleted = true;
                else
                    originalModel.IsCompleted = false;

                _dealContext.Requests.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Request model could not be saved!");
                } 

                var msg = "";
                if (!originalModel.IsCompleted)
                    msg = CreateMessage(ControllerActionType.UnComplete, "request", id);
                else
                    msg = CreateMessage(ControllerActionType.Complete, "request", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Request");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Edit, "request", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Request");
                }
            }
        }
        
        [HttpPost]
        public IActionResult AcceptRequest(Int32 offId, Int32 reqId)
        {
            try
            {
                var offer = _dealContext.Offers.Where(m => m.DeletedAt == null).FirstOrDefault(m => m.Id == offId);
                var subrequest = _dealContext.Requests.Where(m => m.DeletedAt == null).FirstOrDefault(m => m.Id == reqId);
                
                if(offer == null || subrequest == null)
                    throw new Exception("The offermodel and/or requestmodel don't exist anymore!");
                    
                var userId = "";
                if (User.IsSignedIn())
                    userId = User.GetUserId();
                else
                    return RedirectToAction("Index", "Home");

                var usrOffer = _dealContext.Profiles.FirstOrDefault(m => m.UserId == userId);
                var usrSubrequest = _dealContext.Profiles.FirstOrDefault(m => m.UserId == subrequest.UserId);
                
                // Calculate points
                double pointsPerMinute = 1;
                double basepoints = (subrequest.EstimatedTime * pointsPerMinute);
                double bonus = (double)subrequest.Bonus.Value/100;
                double totalBonus = bonus + 1;
                Int32 totalPoints = Convert.ToInt32(basepoints * totalBonus);
                
                // Detract points from usrSubrequest
                usrSubrequest.Points -= totalPoints;
                _dealContext.Profiles.Attach(usrSubrequest);
                _dealContext.Entry(usrSubrequest).State = EntityState.Modified;
                
                // Give points to usrOffer 
                usrOffer.Points += totalPoints;
                _dealContext.Profiles.Attach(usrOffer);
                _dealContext.Entry(usrOffer).State = EntityState.Modified;
                
                // Complete Offer + Request
                offer.IsCompleted = true;
                _dealContext.Offers.Attach(offer);
                _dealContext.Entry(offer).State = EntityState.Modified;
                
                subrequest.IsCompleted = true;
                _dealContext.Requests.Attach(subrequest);
                _dealContext.Entry(subrequest).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Request model could not be saved!");
                } 

                var msg = "";
                msg = CreateMessage(ControllerActionType.Complete, "offer", offId);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = offId, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Offer");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Edit, "offer", offId, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = offId, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Request");
                }
            }
        }
    }
}