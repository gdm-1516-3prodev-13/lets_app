using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Deal.Models;
using Deal.Models.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Authorization;
using System.Security.Claims;
using Microsoft.Extensions.Logging;
using Microsoft.AspNet.Http.Features;
using Microsoft.AspNet.Diagnostics;


namespace Deal.Web.Controllers
{
    [AllowAnonymous]
    public class HomeController : CommonController
    {
        public IActionResult Index()
        {
            var userId = "";
            if (User.IsSignedIn())
                userId = User.GetUserId();
            
            if (userId.Length > 0)
            {
                var model = new UserOverviewViewModel
                {
                    User = _dealContext.Users.Where(m => m.Id == userId).Include(m => m.Profile).FirstOrDefault(),
                    MyGroups = _dealContext.Groups.Where(m => m.Users.Any(u => u.UserId == userId)).Where(m => m.DeletedAt == null).AsEnumerable(),
                    MyRequests = _dealContext.Requests.Where(m => m.UserId == userId).Where(m => m.DeletedAt == null).AsEnumerable(),
                    MyActiveRequests = _dealContext.Requests.Where(m => m.UserId == userId).Where(m => m.IsCompleted == false).Where(m => m.DeletedAt == null).AsEnumerable(),
                    MyCompletedRequests = _dealContext.Requests.Where(m => m.UserId == userId).Where(m => m.IsCompleted == true).Where(m => m.DeletedAt == null).AsEnumerable(),
                    MyOffers = _dealContext.Offers.Where(m => m.UserId == userId).Where(m => m.DeletedAt == null).AsEnumerable(),
                    MyActiveOffers = _dealContext.Offers.Where(m => m.UserId == userId).Where(m => m.IsCompleted == false).Where(m => m.DeletedAt == null).AsEnumerable(),
                    MyCompletedOffers = _dealContext.Offers.Where(m => m.UserId == userId).Where(m => m.IsCompleted == true).Where(m => m.DeletedAt == null).AsEnumerable(),
                };
                
                return View(model);    
            }
            else
            {
                return View();
            }
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            var feature = this.HttpContext.Features.Get<IExceptionHandlerFeature>();
            return View("~/Views/Shared/Error.cshtml", feature?.Error);
        }
    }
}
