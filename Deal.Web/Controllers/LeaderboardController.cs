using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Authorization;
using Microsoft.Data.Entity;
using Deal.Models;
using Deal.Models.ViewModels;

namespace Deal.Web.Controllers
{
    [AllowAnonymous]
    public class LeaderboardController : CommonController
    {
        [HttpGet]
        public IActionResult Index()
        {
            var model = _dealContext.Users.Include(m => m.Profile).OrderByDescending(m => m.Profile.Points).AsEnumerable();

            return View(model);
        }
    }
}