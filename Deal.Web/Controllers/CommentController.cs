using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Authorization;
using Microsoft.Data.Entity;
using Deal.Models;
using Deal.Models.ViewModels;

namespace Deal.Web.Controllers
{
    [Authorize]
    public class CommentController : CommonController
    {   
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(NewCommentViewModel model)
        {
            try 
            {
                if(!ModelState.IsValid)
                    throw new Exception("The Comment is not valid!");
                
                var userId = User.GetUserId();
                if(userId == null || userId == "")
                    return RedirectToAction("Index", "Request");
                    
                var comment = new Comment();
                
                comment.Content = model.Comment.Content;
                comment.UserId = model.UserId;
                
                switch(model.EntityType.ToString())
                {
                    case "Request":
                        var req = _dealContext.Requests.FirstOrDefault(m => m.Id == model.Id);
                        comment.Request = req;
                        break;
                    case "Offer":
                        var ofr = _dealContext.Offers.FirstOrDefault(m => m.Id == model.Id);
                        comment.Offer = ofr;
                        break;
                }

                _dealContext.Comments.Add(comment);
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Comment model could not be saved!");
                }
                
                return RedirectToAction("Details", model.EntityType, new { id = model.Id });
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
            return View();
        }
        
        [HttpPost]
        public IActionResult Delete(Int32 id)
        {
            try
            {
                var originalModel = _dealContext.Comments.FirstOrDefault(m => m.Id == id);
                
                if(originalModel == null)
                    throw new Exception("The existing Comment with id: " + id + " doesn't exist anymore!");
                    
                var userId = User.GetUserId();
                if(originalModel.UserId != userId)
                    return RedirectToAction("Index", "Home");

                originalModel.DeletedAt = DateTime.Now;
                _dealContext.Comments.Attach(originalModel);
                _dealContext.Entry(originalModel).State = EntityState.Modified;
                
                if (_dealContext.SaveChanges() == 0)
                {
                   throw new Exception("The Comment model could not be soft deleted!");
                } 

                var msg = CreateMessage(ControllerActionType.SoftDelete, "comment", id);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 1, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Request");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.SoftDelete, "comment", id, ex);

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Request");
                }
            }
        }
    }
}