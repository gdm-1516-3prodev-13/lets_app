using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using System.Security.Claims;
using Deal.Models;
using Deal.Models.ViewModels;
using Deal.Data;

namespace Deal.Web.ViewComponents
{
    public class AddCommentViewComponent : ViewComponent
    {
        private readonly DealDbContext _dealContext;
        
        public AddCommentViewComponent(DealDbContext dealContext)
        {
            _dealContext = dealContext;
        }
        
        public IViewComponentResult Invoke(string entityType, int? id)
        {
            var userId = HttpContext.User.GetUserId();
            
            var newComment = new NewCommentViewModel
            {
                Id = id,
                UserId = userId,
                Comment = new Comment()
            };
            
            switch(entityType)
            {
                case "Request":
                    newComment.EntityType = "Request";
                    break;
                case "Offer":
                    newComment.EntityType = "Offer";
                    break;
            }
            
            return View(newComment);
        }
    }
}